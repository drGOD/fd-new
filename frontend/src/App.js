import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    Link,
    useParams,
    withRouter
} from "react-router-dom";
// import logo from './logo.svg';
import { PrivateRoute } from './components/helpers/PrivateRoute';
import { PrivateComponent } from './components/helpers/PrivateComponent';
import { AuthorizedProtectedRoute } from './components/helpers/AuthorizedProtectedRoute';
import './App.scss';
import Header from "./components/Header/index";
import Menu from "./components/Menu/index";
import MyListsOLD from "./components/MyLists OLD";
import AddModel from "./components/add_model OLD";
import ModelPage from "./components/model_page OLD";
import {
    PATH_HOME, PATH_DATASETS,
    PATH_NEW_DATASET, PATH_DATASET, PATH_MODELS,
    PATH_NEW_MODEL, PATH_MODEL, PATH_PROFILE, PATH_AUTH, PATH_MAIN
}
    from './router/index';
import Auth from "./views/Auth";
import Datasets from "./views/Datasets";
import Profile from "./views/Profile";
import Models from "./views/Models";
import NewDataset from "./views/NewDataset";
import NewModel from "./views/NewModel";
import Dataset from "./views/Dataset";
import Model from "./views/Model";
import Main from './views/Main';
import store from './store'
import {observer} from "mobx-react";
import {url} from "./urls";
import axios from "axios";
import Cookies from 'universal-cookie';

const cookies = new Cookies;

class App extends React.Component {
    UNSAFE_componentWillMount() {
        store.setCurrentPage(window.location.pathname);
        const cookies = new Cookies;
        if (cookies.get('token_fdh')) {
            store.setToken(cookies.get('token_fdh'));
            this.getDatasets();
            this.getModels();
            const new_url = url + "user/"
            axios
                .get(new_url, { headers:
                        { Authorization: 'Token '+ cookies.get('token_fdh') } })
                .then(response => {
                    store.updateCurrentUser(response.data); //получаем информацию о текущем авторизованном пользователе и записываем в стор
                    store.setCurrentUserId(response.data.id);
                })
                .catch((error) => {

                })
        }
    }
    getDatasets() { //используем чтобы при входе в приложение уже можно было искать датасеты (затем обновление происходит на странице Datasets)
        let token = store.token;
        let new_url = url + "datasets/";
        // this.setState({loading: true});
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // for (let el in response.data) {
                //     let curr_res = response.data[el];
                //     new_url = url + "users/"+curr_res.user_id+"/"
                //     axios
                //         .get(new_url)
                //         .then(response => {
                //             curr_res.username = response.data.username;
                //         })
                //         .catch((error) => {
                //             curr_res.username = "";
                //         })
                //         .finally(()=> {
                //             res.push(curr_res);
                //             store.setDatasets(res);
                //             // this.setState({datasets: res});
                //         })
                // }
                store.setDatasets(response.data);
                // this.setState({loading: false});
            })
            .catch((error) => {
                // this.setState({loading: false});
                // this.setState({error: true});
            })
    }
    getModels() { //используем чтобы при входе в приложение уже можно было искать модели (затем обновление происходит на странице Models)
        let token = store.token;
        let new_url = url + "models/";
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // for (let el in response.data) {
                //     let curr_res = response.data[el];
                //     new_url = url + "users/"+curr_res.user_id+"/"
                //     axios
                //         .get(new_url)
                //         .then(response => {
                //             curr_res.username = response.data.username;
                //         })
                //         .catch((error) => {
                //             curr_res.username = "";
                //         })
                //         .finally(()=> {
                //             res.push(curr_res);
                //             store.setModels(res);
                //         })
                // }
                store.setModels(response.data);
            })
            .catch((error) => {
            })
    }

    render() {
            return (
                <div className="app">
                    {/*{store.currentPage !== PATH_AUTH ? <Header/> : null}*/}

                    <Router>
                    {/*<Router>*/}
                        <PrivateComponent path={PATH_HOME} component={Header}/>
                    {/*</Router>*/}
                    <div className="app_container">
                        {/*<Router>*/}
                        {/*<PrivateRoute path={PATH_HOME} component={Menu}/>*/}
                        <PrivateComponent path={PATH_HOME} component={Menu}/>
                        {/*</Router>*/}
                        {/*<PrivateComponent path={PATH_HOME} component={Menu}/>*/}
                        {/*{store.currentPage !== PATH_AUTH ? <Menu/> : null}*/}
                        {/*<Router>*/}
                        <Switch>
                            <AuthorizedProtectedRoute exact path={PATH_AUTH} component={Auth}/>
                            {/*<PrivateComponent exact path={PATH_HOME} component={Header}/>*/}
                            <PrivateRoute exact path={PATH_HOME} component={Main}/>
                            {/*<PrivateRoute exact path = {PATH_MAIN} component = {Main} />*/}
                            <PrivateRoute exact path={PATH_DATASETS} component={Datasets}/>
                            <PrivateRoute exact path={PATH_NEW_DATASET} component={NewDataset}/>
                            <PrivateRoute exact path={PATH_DATASET} component={Dataset}/>
                            <PrivateRoute exact path={PATH_MODELS} component={Models}/>
                            <PrivateRoute exact path={PATH_NEW_MODEL} component={NewModel}/>
                            <PrivateRoute exact path={PATH_MODEL} component={Model}/>
                            <PrivateRoute exact path={PATH_PROFILE} component={Profile}/>
                        {/*</Router>*/}
                        </Switch>
                    </div>
                </Router>
                </div>
            )
        }
}

// export default withRouter(App);
export default observer(App);
