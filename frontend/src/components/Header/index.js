import React from 'react';
import {
    Link
} from "react-router-dom";
import './style.scss';
import store from '../../store'
import InputSearch from '../Inputs/Search/index';
import {
    PATH_HOME, PATH_DATASETS,
    PATH_NEW_DATASET, PATH_MODELS,
    PATH_NEW_MODEL, PATH_PROFILE, PATH_AUTH
}
    from '../../router/index';
import axios from "axios";
import {url} from "../../urls";
import {observer} from "mobx-react";
import Cookies from 'universal-cookie';

const cookies = new Cookies;

interface IState {
    username?: string;
}
class Header extends React.Component<IState> {

    state = {
    };
    constructor() {
        super();
        this.state = {
            username: String
        };
    }

    componentDidMount() {
        if (window.location.pathname!==PATH_AUTH) {
            this.onMount();
        }
    }
    onMount() {
        const new_url = url + "user/"
        axios
            .get(new_url, { headers:
                    // { Authorization: 'Token '+ store.token } })
                    { Authorization: 'Token '+ cookies.get('token_fdh') } })
            .then(response => {
                this.setState({username: response.data.username});
                store.updateCurrentUser(response.data);
                // alert("SUCCESS");
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!HEADER!!!");
            })
    }
    render() {
        return (
            <div className="header">
                <Link to={PATH_HOME} className="link">
                  <span className="logo" href="#">
                    FinDataHub
                  </span>
                </Link>
                <div className="btns">
                    <Link to={PATH_DATASETS} className="link">
                        <button className={store.currentPage === PATH_DATASETS ? 'nav selected' : 'nav'}>Датаcеты</button>
                    </Link>
                    <Link to={PATH_NEW_DATASET} className="link">
                        <button className={store.currentPage === PATH_NEW_DATASET ? 'nav add selected' : 'nav add'}>+</button>
                    </Link>
                    <Link to={PATH_MODELS} className="link">
                        {store.currentPage === PATH_MODELS}
                        <button className={store.currentPage === PATH_MODELS ? 'nav selected' : 'nav'}>Модели</button>
                    </Link>
                   <Link to={PATH_NEW_MODEL} className="link">
                       <button className={store.currentPage === PATH_NEW_MODEL ? 'nav add selected' : 'nav add'}>+</button>
                   </Link>
                </div>
                <div className="group">
                    <InputSearch className="search" />
                    <Link to={PATH_PROFILE} className="link">
                        <button className={store.currentPage === PATH_PROFILE ? 'nav user selected' : 'nav user'}>
                            <img src="../../../icons/user_icon_white.svg" />
                            <span>{this.state.username}</span>
                        </button>
                    </Link>
                </div>
            </div>
            // <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{border: "1px solid red"}}>
            //     <a className="navbar-brand" href="#">FinDataHub</a>
            //     <button className="navbar-toggler" type="button" data-toggle="collapse"
            //             data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            //             aria-expanded="false" aria-label="Toggle navigation">
            //         <span className="navbar-toggler-icon"></span>
            //     </button>
            //
            //     <div className="collapse navbar-collapse" id="navbarSupportedContent">
            //         <ul className="navbar-nav mr-auto">
            //             <li className="nav-item active">
            //                 <Link to="/"><span className="nav-link" href="#">Home</span></Link>
            //             </li>
            //             <li className="nav-item active">
            //                 <Link to="/models"><span className="nav-link" href="#">Все модели</span></Link>
            //             </li>
            //             <li className="nav-item active">
            //                 <Link to="/datasets"><span className="nav-link" href="#">Все датасеты</span></Link>
            //             </li>
            //
            //             <li className="nav-item dropdown">
            //                 <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
            //                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            //                     Действия
            //                 </a>
            //                 <div className="dropdown-menu" aria-labelledby="navbarDropdown">
            //                     <Link to="/add_dataset">
            //                         <button className="dropdown-item" href="#">Загрузить датасет</button>
            //                     </Link>
            //                     <Link to="/add_model">
            //                         <button className="dropdown-item" href="#">Загрузить модель</button>
            //                     </Link>
            //                     {/*<div className="dropdown-divider"></div>*/}
            //                     {/*<a className="dropdown-item" href="#">Something else here</a>*/}
            //                 </div>
            //             </li>
            //
            //             {/*<li className="nav-item">*/}
            //             {/*    <a className="nav-link disabled" href="#">Disabled</a>*/}
            //             {/*</li>*/}
            //         </ul>
            //
            //         <form className="form-inline my-2 my-lg-0">
            //             <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"></input>
            //             <button className="btn btn-outline-success my-2 my-sm-0 mr-sm-2" type="submit">Search</button>
            //             <Link to="/login"><button className="btn btn-outline-primary mr-sm-2">Войти</button></Link>
            //             <Link to="/register"><button className="btn btn-outline-primary mr-sm-2">Зарегистрироваться</button></Link>
            //         </form>
            //
            //     </div>
            // </nav>
        );
    }
}

export default observer(Header);
