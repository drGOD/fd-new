import React from 'react';
import './style.scss';
import {array} from "prop-types";


interface Props {
    text: string, // заголовок инпута
    name?: string,
    id?: string,
    required?: boolean, // обязательный ли к заполнению инпут (true || false)
    placeholder: String, // текст-подсказка внутри пустого инпута
}
interface IState {

}

class TextArea extends React.Component<Props, IState> {

    static defaultProps = {
        required: true
    };
    state: IState;
    constructor(props) {
        super(props);

        this.state = {

        };
        this.updateInput = this.updateInput.bind(this)
    }

    updateInput = (e) => {
        this.props.sendValue(e.target.value);
    }

    render() {
        return (
            <div
                className="textarea"
            >
                <span>{this.props.text}</span>
                <textarea name={this.props.name}
                          id={this.props.id}
                          required={this.props.required}
                          placeholder={this.props.placeholder}
                          onChange={this.updateInput}
                >
                </textarea>
            </div>
        );
    }
}

export default TextArea;
