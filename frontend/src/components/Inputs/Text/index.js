import React from 'react';
import './style.scss';

interface Props {
    text: String, // заголовок инпута
    type?: String, // тип ипута (text || number || email || password)
    required?: boolean, // обязательный ли к заполнению инпут (true || false)
    name?: String, //мб
    id?: String, //мб
    placeholder: String, // текст-подсказка внутри пустого инпута
    // autocomplete: String, // сохранение предыдущих введенных значений (on || off)
    min?: Number, // минимальное значения для input number
    max?: Number, // максимальное значения для input number
    step?: String, // разрешение на ввод дробных чисел (any)
    pattern?: String, // regex для type=text
    style?: String, // light || dark стиль инпута
    defaultValue?: String, // значение инпута по умолчанию
    // sendValue: (value) => void
    width?: Number // ширина инпута
}

interface IState {

}

class InputText extends React.Component<Props, IState> {

    static defaultProps = {
        type: "text",
        required: true,
        autocomplete: "on",
        pattern: null,
        style: "light",
        defaultValue: null,
        width: 300
    };
    state: IState;
    constructor(props) {
        super(props);

        this.state = {

        };
        this.updateInput = this.updateInput.bind(this)
    }
    componentDidMount() {
        // this.setState({value: this.props.value})
        if (this.props.defaultValue || this.props.defaultValue===0) {
            this.props.sendValue(this.props.defaultValue);
        }
    }

    updateInput = (e) => {
        this.props.sendValue(e.target.value);
    }

    render() {
        return (
            <div
                 className={this.props.style === "light"? "input_text light" : "input_text dark"}
                 style={{"width":this.props.width+"px"}}
            >
                <div className="container">
                    <span className="text">{ this.props.text }</span>
                    {this.props.required ? <div className="circle"></div> : null}
                </div>
                <input
                    type={this.props.type}
                    name={this.props.name}
                    id={this.props.id}
                    placeholder={this.props.placeholder}
                    defaultValue={this.props.defaultValue}
                    required={this.props.required}
                    pattern={this.props.type==='text' ? this.props.pattern : null}
                    // autoComplete={this.props.type === 'text' ? this.props.autocomplete : null}
                    min={this.props.type === 'number' ? this.props.min : null}
                    max={this.props.type === 'number' ? this.props.max : null}
                    step={this.props.type === 'number' && this.props.step === 'any' ? this.props.step : null}
                    onChange={this.updateInput}
                />
            </div>
        );
    }
}

export default InputText;
