import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import {PATH_DATASETS, PATH_MODELS} from "../../../router";
import './style.scss';
import store from '../../../store'

class InputSearch extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
          models: [],
          datasets: [],
          showResults: false
        }
        this.onSearch = this.onSearch.bind(this);
        this.onClick = this.onClick.bind(this);
      }


    onSearch = (e) => {

        // console.log("MODELS НАЙДЕННЫЕ:");
        let newArray1 = store.models.filter(function (el) {
            // return el.price <= 1000 &&
            //     el.sqft >= 500 &&
            //     el.num_of_beds >=2 &&
            //     el.num_of_baths >= 2.5;
            return el.name.toLowerCase().includes(e.target.value.toLowerCase());
        }).slice(0,3);

        // console.log("DATASETS НАЙДЕННЫЕ:");
        let newArray2 = store.datasets.filter(function (el) {
            // return el.price <= 1000 &&
            //     el.sqft >= 500 &&
            //     el.num_of_beds >=2 &&
            //     el.num_of_baths >= 2.5;
            return el.name.toLowerCase().includes(e.target.value.toLowerCase());
        }).slice(0,3);

        // for (let i in newArray2) {
        //     console.log("   " + newArray2[i].name);
        // }
        // console.log("---------------------------------");

        this.setState({models:newArray1, datasets:newArray2});
        this.setState({ showResults: true });
    }

    onClick() {
        this.setState({ showResults: false });
        document.getElementById("searchInput").value = "";
    }

    render() {
        return (
            <div className="search">
                <input id="searchInput" type="text" onChange={this.onSearch}/>
                <img className="searchImg" src="../../../icons/loupe_white.svg"/>
                { this.state.showResults ?
                <div className="searchResult">
                    <div className="triangle"></div>
                    <div className="half">
                        <h3>Модели</h3>
                        <span className="button" onClick={this.onClick}><img src="../../../icons/close.svg"/></span>
                        {
                            this.state.models.length===0 ? <div className="row empty">Нет результатов</div> :
                            this.state.models.map((e, i) =>
                            (<div className="row" key={e.id}>
                                    <div className="top">
                                        <Link to={PATH_MODELS + "/" + e.id} className="link">
                                            <div className="object_name" onClick={this.onClick}>{e.name}</div>
                                        </Link>
                                        <div className="billing">
                                            <img src="../../../icons/billing.svg"/>
                                            <div className="money">{parseFloat(e.price).toFixed(2)}</div>
                                        </div>
                                    </div>
                                    <div className="bottom">
                                        <div className="owner">
                                            <img src="../../../icons/user_icon_dark_blue.svg"/>
                                            <div className="name">{e.username}</div>
                                        </div>
                                    </div>
                                </div>
                                )
                            )
                        }
                    </div>
                    <div className="half ds">
                        <h3>Датасеты</h3>
                        {
                            this.state.datasets.length===0 ? <div className="row empty">Нет результатов</div> :
                            this.state.datasets.map((e, i) =>
                            (<div className="row" key={e.id}>
                                    <div className="top">
                                        <Link to={PATH_DATASETS + "/" + e.id} className="link">
                                            <div className="object_name" onClick={this.onClick}>{e.name}</div>
                                        </Link>
                                    </div>
                                    <div className="bottom">
                                        <div className="owner">
                                            <img src="../../../icons/user_icon_dark_blue.svg"/>
                                            <div className="name">{e.username}</div>
                                        </div>
                                    </div>
                                </div>
                            )
                        )
                        }
                    </div>
                </div>
                : null }
            </div>
        );
    }
}

export default InputSearch;
