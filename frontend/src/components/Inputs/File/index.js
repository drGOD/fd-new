import React from 'react';
import './style.scss';
import {array} from "prop-types";


interface Props {
    formats: string, //форматы файлов через запятую, например: "xlsx, xls" (на данный момент поддерживается только csv и py)
    name?: string,
    id?: string,
    required?: boolean // обязательный ли к заполнению инпут (true || false)
}
interface IState {
    currentFile: string
}

class File extends React.Component<Props, IState> {

    static defaultProps = {
        required: true
    };
    state: IState;
    constructor(props) {
        super(props);

        this.state = {
            currentFile: "Choose file..."
        };
        this.updateInput = this.updateInput.bind(this)
        this.getFormats = this.getFormats.bind(this)
        this.onFileChange = this.onFileChange.bind(this)
    }
    // componentDidMount() {
    //     this.setState({value: this.props.value})
    // }

    updateInput = (e) => {
        this.props.sendValue(e.target.value);
    }
    getFormats() {
        let result = "";
        // console.log(result);
        if (this.props.formats.indexOf("py") !== -1) {
            result += ".py";
        }
        if (this.props.formats.indexOf("csv") !== -1) {
            if (result.length === 0) {
                result += ".csv";
            } else {
                result += ", .csv";
            }
        }
        if (this.props.formats.indexOf("zip") !== -1) {
            if (result.length === 0) {
                result += "application/x-compressed, application/x-zip-compressed, application/zip, multipart/x-zip";
            } else {
                result += ", application/x-compressed, application/x-zip-compressed, application/zip, multipart/x-zip";
            }
        }
        return result;
    }
    onFileChange(e) {
        let fileName = e.target.files[0].name;
        this.setState({currentFile: fileName})

        // this.props.sendValue(e.target.value);
        this.props.sendValue(this.refs.file.files[0]);
    }

    render() {
        return (
            <label
                className="file"
                for={this.props.id}
                title={this.state.currentFile}
            >
                <input
                className="input"
                required={this.props.required}
                type="file"
                name={this.props.name}
                accept={this.getFormats()}
                id={this.props.id}
                onChange={this.onFileChange}
                ref="file"
                />
                <div className="helper"></div>
                <span className="text">{ this.state.currentFile }</span>
    </label>
        );
    }
}

export default File;
