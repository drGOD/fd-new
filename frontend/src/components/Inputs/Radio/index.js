import React from 'react';
import './style.scss';

interface Props {
    text: String, // текст
    name?: String,
    value?: String,
    id?: String
}

class Radio extends React.Component<Props> {

    static defaultProps = {
        // type: "submit"
    };
    updateRadio = (e) => {
        // console.log("radio: ");
        // console.log(e.target.value);
        // console.log(e.target.checked);
        this.props.sendValue(e.target.checked);
    }
    render() {
        return (
            <div className="radio">
                <input type="radio"
                       value={this.props.value}
                       name={this.props.name}
                       id={this.props.id}
                       onChange={this.updateRadio}/>
                       <label htmlFor={this.props.id}>{this.props.text}</label>
            </div>
            // <label className="radio">
            //     <input type="radio"
            //            value={this.props.value}
            //            name={this.props.name}
            //            onChange={this.updateRadio}/>
            //     <div>{this.props.text}</div>
            // </label>

        );
    }
}

export default Radio;
