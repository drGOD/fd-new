import React from 'react';
import './style.scss';

interface Props {
    text: String, // текст
    name?: String,
    id: String,
    default?: boolean
}

class Checkbox extends React.Component<Props> {

    static defaultProps = {
        default: false
    };

    updateCheckbox = (e) => {
        // console.log("radio: ");
        // console.log(e.target.value);
        // console.log(e.target.checked);
        this.props.sendValue(e.target.checked);
    }
    render() {
        return (
            <div className="checkbox">
                <input
                    type="checkbox"
                    name={this.props.name}
                    id={this.props.id}
                    onChange={this.updateCheckbox}
                    defaultChecked={this.props.default}
            />
        <label for={this.props.id}>{ this.props.text }</label>
    </div>
        );
    }
}

export default Checkbox;
