import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

class HeaderOLD extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{border: "1px solid red"}}>
                <a className="navbar-brand" href="#">FinDataHub</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link to="/"><span className="nav-link" href="#">Home</span></Link>
                        </li>
                        <li className="nav-item active">
                            <Link to="/models"><span className="nav-link" href="#">Все модели</span></Link>
                        </li>
                        <li className="nav-item active">
                            <Link to="/datasets"><span className="nav-link" href="#">Все датасеты</span></Link>
                        </li>

                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Действия
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <Link to="/add_dataset">
                                    <button className="dropdown-item" href="#">Загрузить датасет</button>
                                </Link>
                                <Link to="/add_model">
                                    <button className="dropdown-item" href="#">Загрузить модель</button>
                                </Link>
                                {/*<div className="dropdown-divider"></div>*/}
                                {/*<a className="dropdown-item" href="#">Something else here</a>*/}
                            </div>
                        </li>

                        {/*<li className="nav-item">*/}
                        {/*    <a className="nav-link disabled" href="#">Disabled</a>*/}
                        {/*</li>*/}
                    </ul>

                    <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"></input>
                            <button className="btn btn-outline-success my-2 my-sm-0 mr-sm-2" type="submit">Search</button>
                        <Link to="/login"><button className="btn btn-outline-primary mr-sm-2">Войти</button></Link>
                        <Link to="/register"><button className="btn btn-outline-primary mr-sm-2">Зарегистрироваться</button></Link>
                    </form>

                </div>
            </nav>
        );
    }
}

export default HeaderOLD;
