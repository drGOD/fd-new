import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import './style.scss';
import store from "../../store";
import {PATH_DATASETS, PATH_MODELS, PATH_NEW_DATASET} from "../../router";
import axios from "axios";
import {url} from "../../urls";
import Cookies from 'universal-cookie';
import {array} from "prop-types";
import {observer} from "mobx-react";

const cookies = new Cookies;

interface IState {
    datasets: array;
    models: array;
    subs: array;
    subs_models: array,
    example: array,
    modelsLoaded: boolean,
    datasetsLoaded: boolean,
    subsLoaded: boolean
}
@observer
class Menu extends React.Component<IState> {
    state = {
    };
    constructor() {
        super();
        this.state = {
            datasets: [],
            models: [],
            subs: [],
            subs_models: [],
            example: [],
            modelsLoaded: false,
            datasetsLoaded: false,
            subsLoaded: false
        };
        // this.getModelsById = this.getModelsById.bind(this);
    }

    UNSAFE_componentWillMount() {
        this.getDatasets();
        this.getModels();
        this.getSubs();
    }

    getDatasets() {
        let token = store.token;
        const new_url = url + "user/datasets/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // alert("SUCCESS DATASETS");
                this.setState(
                {datasets: response.data});
                store.setCurrentUserDatasets(response.data);
                this.setState({datasetsLoaded: true});
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!MENU!!!DATASETS");
            })
        // this.setState({datasets: [ //FAKE DATA
        //         {
        //             "id": 1,
        //             "name" : "Датасет",
        //             "user_id": 3,
        //             "desc" : "Описание датасета описание датасета описание датасета",
        //             "is_private": false,
        //             "dt": "что-то"
        //         },
        //         {
        //             "id": 2,
        //             "name" : "Второй датасет",
        //             "user_id": 3,
        //             "desc" : "Описание датасета описание датасета описание датасета",
        //             "is_private": false,
        //             "dt": "что-то"
        //         },
        //         {
        //             "id": 3,
        //             "name" : "Третий датасет",
        //             "user_id": 3,
        //             "desc" : "Описание датасета описание датасета описание датасета",
        //             "is_private": false,
        //             "dt": "что-то"
        //         }
        //     ]});
    }
    getModels() {
        let token = store.token;
        const new_url = url + "user/models/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // alert("SUCCESS MODELS");
                this.setState({models: response.data});
                store.setCurrentUserModels(response.data);
                this.setState({modelsLoaded: true});
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!MENU!!!MODELS");
            })
        // this.setState({models: [ //FAKE DATA
        //         {
        //             "id": 1,
        //             "name" : "Модель",
        //             "user_id": 3,
        //             "desc" : "Описание модели описание модели",
        //             "version" : "15",
        //             "price" : "700"
        //         },
        //         {
        //             "id": 2,
        //             "name" : "Еще модель",
        //             "user_id": 3,
        //             "desc" : "Описание модели описание модели",
        //             "version" : "15",
        //             "price" : "1500"
        //         },
        //         {
        //             "id": 3,
        //             "name" : "Модель модель",
        //             "user_id": 3,
        //             "desc" : "Описание модели описание модели",
        //             "version" : "15",
        //             "price" : "10000"
        //         }
        //     ]});
    }
    getSubs() {
        let token = store.token;
        const new_url = url + "user/subs/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                this.setState({subs: response.data}, function() {
                        let arr = [];
                        let subs = this.state.subs;
                        if (subs.length !== 0) {
                            for (let key in subs) {
                                arr.push(subs[key].sci_model_id);
                            }
                            this.getModelsById(arr);
                        }
                        else {
                            store.setCurrentUserSubs(subs);
                            this.setState({subsLoaded: true});
                        }
                    });
            })
            .catch((error) => {
            })
        // this.setState({subs: [ //FAKE DATA
        //         {
        //             "id": 1,
        //             "user_id": 3,
        //             "sci_model_id" : 1,
        //             "dt" : "smth"
        //         },
        //         {
        //             "id": 2,
        //             "user_id": 3,
        //             "sci_model_id" : 2,
        //             "dt" : "smth"
        //         }
        //     ]}, function() {
        //     let arr = [];
        //     let subs = this.state.subs;
        //     for (let key in subs) {
        //         arr.push(subs[key].sci_model_id);
        //     }
        //     this.getModelsById(arr);}
        // );
    }
    getModelsById(arr) {
        for (let key in arr) {
            const new_url = url + "models/"+arr[key]+"/";
            axios
                .get(new_url)
                .then(response => {
                    // alert("SUCCESS MODELS BY ID");
                    let subs_models = this.state.subs_models;
                    subs_models.push(response.data);
                    this.setState({subs_models: subs_models});
                    store.setCurrentUserSubs(subs_models);
                    this.setState({subsLoaded: true});
                })
                .catch((error) => {
                    // alert("FAILURE!!!!!!MENU!!!MODELS BY ID");
                })
            // let subs_models = this.state.subs_models;
            // subs_models.push({
            //     "id": arr[key],
            //     "name" : "fake_model_"+arr[key],
            //     "user_id": 0,
            //     "desc" : "ОПИСАНИЕ ОПИСАНИЕ ОПИСАНИЕ ОПИСАНИЕ ОПИСАНИЕ ОПИСАНИЕ ОПИСАНИЕ",
            //     "version" : 1,
            //     "price" : 15000
            // });
            // this.setState({subs_models: subs_models});
        }
    }
    render() {
        return (
            <div className="menu">
                <div className='my_datasets block'>
                    <div className="my_header">
                        <div>Мои датасеты</div>
                        <img src="../../../icons/menu_arrow.svg"/>
                    </div>
                        <div className="rows">
                            { this.state.datasetsLoaded &&
                                store.currentUserDatasets.map((e,i) =>
                                    (<Link to={PATH_DATASETS+"/"+e.id} className="link" key={e.id}>
                                        <div className="row">
                                            <div className="number">{i+1}</div>
                                            <div className="name">{e.name}</div>
                                        </div>
                                    </Link>) )
                            }
                        </div>
                </div>
                <div className='my_models block'>
                    <div className="my_header">
                        <div>Мои модели</div>
                        <img src="../../../icons/menu_arrow.svg"/>
                    </div>
                    <div className="rows">
                        {/*{*/}
                        {/*    this.state.models.map((e,i) =>*/}
                        {/*        (<Link to={PATH_MODELS+"/"+e.id} className="link" key={e.id}>*/}
                        {/*            <div className="row">*/}
                        {/*                <div className="number">{i+1}</div>*/}
                        {/*                <div className="name">{e.name}</div>*/}
                        {/*            </div>*/}
                        {/*        </Link>) ) }*/}
                                {
                                    this.state.modelsLoaded &&
                            store.currentUserModels.map((e,i) =>
                                (<Link to={PATH_MODELS+"/"+e.id} className="link" key={e.id}>
                                    <div className="row">
                                        <div className="number">{i+1}</div>
                                        <div className="name">{e.name}</div>
                                    </div>
                                </Link>) ) }
                    </div>
                </div>
                <div className='my_subs block'>
                    <div className="my_header">
                        <div className="container">
                            <img src="../../../icons/star_blue.png"/>
                            <div>Подписки</div>
                        </div>
                        <img src="../../../icons/menu_arrow.svg"/>
                    </div>
                    <div className="rows">
                        {this.state.subsLoaded &&
                            store.currentUserSubs.map((e,i) =>
                                (<Link to={PATH_MODELS+"/"+e.id} className="link" key={e.id}>
                                    <div className="row">
                                        <div className="number">{i+1}</div>
                                        <div className="name">{e.name}</div>
                                    </div>
                                </Link>) ) }
                    </div>
                </div>
            </div>
        );
    }
}

export default Menu;
