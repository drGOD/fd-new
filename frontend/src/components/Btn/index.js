import React from 'react';
import './style.scss';

interface Props {
    text: String, // текст в кнопке
    type?: String, // тип кнопки (submit || reset || button)
    loading?: Boolean, // при true делает кнопку disabled (true || false)
    spinner?: Boolean, // нужен ли спиннер при loading=true (true || false)
    color?: String // светлая/темная/красная кнопка (light || dark || red || green)
}

class InputText extends React.Component<Props> {

    static defaultProps = {
        type: "submit",
        loading: false,
        spinner: false,
        color: 'dark'
    };
    render() {
        return (
            <button
                    className={'btn ' + this.props.color}
                    type={this.props.type}
                    disabled={this.props.loading}>
                    <span
                        style={this.props.loading && this.props.spinner ? {color:'transparent'} : {}}>
                        { this.props.text }
                    </span>
                    {
                        this.props.loading && this.props.spinner ?
                            <div className="spinner">
                                <svg viewBox="0 0 50 50">
                                    <circle
                                        className="path"
                                        cx="25"
                                        cy="25"
                                        r="15"
                                        fill="none"
                                        stroke-width="5"
                                    ></circle>
                                </svg>
                            </div> : null
                    }
    </button>
        );
    }
}

export default InputText;
