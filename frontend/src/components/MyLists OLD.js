import React from 'react';
import MyListOLD from "./MyList OLD";

class MyListsOLD extends React.Component {

    render() {
        return (
            <div style={{border: "1px solid green"}}>
                        <MyListOLD type="models" settings={this.props.settings}></MyListOLD>
                        <MyListOLD type="datasets" settings={this.props.settings}></MyListOLD>
                        <MyListOLD type="subs" settings={this.props.settings}></MyListOLD>
            </div>
        );
    }
}

export default MyListsOLD;
