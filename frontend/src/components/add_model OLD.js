import React from 'react';

class AddModel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            desc: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.send_data = this.send_data.bind(this);
    }

    handleChange(event) {
        let s = this.state;
        s[event.target.name] = event.target.value;
        this.setState(s);
    }

    send_data(event) {
        event.preventDefault();

        console.log(1);

        const formData = new FormData();
        const fileField = document.querySelector('input[type="file"]');

        formData.append('model_container', fileField.files[0]);

        let filename = this.props.settings["API_PATHS"]["add_model"];
        fetch(filename, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            },
            body: formData,
        })
            .then((res) => res.json())
            .then(
                (data) => {
                    // console.log(2);
                    // console.log(data);
                    alert("Модель успешно добавлена");
                    //  редирект на страницу модели по полученному id
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                    console.log(3);
                    console.log(this.state);
                }
            );
    }

    render() {
        return (
            <div style={{border: "1px solid pink"}}>
                <h1>Добавление модели</h1>
                <form onSubmit={this.send_data}>
                    <div className="form-group">
                        <label htmlFor="name">Название:</label>
                        <input type="text" value={this.state.name} onChange={this.handleChange} className="from-control"
                               id="name" name="name" placeholder="Название модели..."/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="desc">Описание:</label>
                        <textarea value={this.state.desc} onChange={this.handleChange} className="from-control"
                                  id="desc" name="desc" placeholder="Введите подробное описание модели..."/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="file">Файл с кодом:</label>
                        <input type="file" className="from-control" id="file"/>
                    </div>
                    <button type="submit">Опубликовать</button>
                </form>
            </div>
        );
    }
}

export default AddModel;
