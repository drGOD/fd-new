import React from 'react';
import {Link} from "react-router-dom";

class MyListOLD extends React.Component {
    state = {
        error: null,
        isLoaded: false,
        items: [],
    };

    componentDidMount() {
        let filename = this.props.settings["API_PATHS"][this.props.type];
        fetch(filename, {
            headers: {
                'Accept': 'application/json'
            }
        })
            .then((res) => res.json())
            .then(
                (data) => {
                    // console.log(1);
                    // console.log(data);
                    this.setState({
                        isLoaded: true,
                        items: data.models
                    });
                    // this.render();
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );

        return {
            models: {
                "id": "1",
                "name": "Тестовая модель",
            },
        }
    }

    render() {
        let heading = {
            models: "Модели",
            datasets: "Датасеты",
            subs: "Подписки",
        };
        let type = this.props.type;
        let colapseId = `collapse_${type}`;
        let colapseHref = `#collapse_${type}`;
        let obj = this.props.settings['type2obj'][type];

        return (
            <div className="container" style={{border: "1px solid blue"}} >

                <div className="card">
                    <div className="card-header panel-heading">
                        {/*<a className="accordion-toggle"*/}
                        {/*   data-toggle="collapse"*/}
                        {/*   href={colapseHref}>*/}
                            Мои {heading[type]}
                        {/*</a>*/}
                        <a className="float-right"
                                data-toggle="collapse"
                                href={colapseHref}
                                aria-expanded="true"
                                aria-controls={colapseId}>
                            <svg xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                        </a>
                    </div>

                    <div className="card-body collapse show" id={colapseId}>
                        <table className="table table-hover table-condensed table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.items.map(item => (
                                    // **/
                                    <tr key={item.key}>
                                        <th scope="row">{item.key}</th>
                                        <td>
                                            <Link to={"/" + obj + "/" + item.key}>
                                                {item.name}
                                            </Link>
                                        </td>
                                        <td></td>
                                    </tr>
                                    // </Link>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }
}

export default MyListOLD;
