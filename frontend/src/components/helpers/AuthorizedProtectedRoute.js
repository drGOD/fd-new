import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import Cookies from 'universal-cookie';
import {PATH_HOME, PATH_MAIN} from "../../router";

const cookies = new Cookies;

export const AuthorizedProtectedRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            cookies.get('token_fdh') ?
                <Redirect
                    // to={{pathname:PATH_HOME, state:{from: props.location}}}
                    // to={{pathname:PATH_MAIN}}
                    to={{pathname:PATH_HOME}}
                />
             :
                <Component {...props} />
        }
    />
);
