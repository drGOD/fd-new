import React from 'react';
import {Route} from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies;

export const PrivateComponent = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            cookies.get('token_fdh') ?
                <Component {...props} />
             :

                null
        }
    />
);
