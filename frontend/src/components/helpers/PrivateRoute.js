import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import Cookies from 'universal-cookie';
import {PATH_AUTH, PATH_MAIN} from "../../router";
import store from "../../store";

const cookies = new Cookies;

export const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            cookies.get('token_fdh') ?
                <Component {...props} />
             :

                <Redirect
                    // to={{pathname:PATH_AUTH, state:{from: props.location}}}
                    to={{pathname:PATH_AUTH}}
                    // to={{pathname:PATH_MAIN}}
                />
        }
    />
);
