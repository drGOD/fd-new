import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import './style/_variables.scss';
import './style/_fonts.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter } from "react-router-dom";
import {createBrowserHistory} from 'history';
import axios from "axios";
import Cookies from 'universal-cookie';
import store from "./store";
import {url} from "./urls";

// const cookies = new Cookies;
// if (cookies.get('token_fdh')) {
//     store.setToken(cookies.get('token_fdh'));
//
//     const new_url = url + "user/"
//     axios
//         .get(new_url, { headers:
//                 { Authorization: 'Token '+ cookies.get('token_fdh') } })
//         .then(response => {
//             store.updateCurrentUser(response.data);
//             console.log('updated user info');
//             console.log(store.currentUser.id);
//         })
//         .catch((error) => {
//
//         })
// }

const history = createBrowserHistory()

ReactDOM.render((<BrowserRouter>
    <Route path="/" component={ App }/>
</BrowserRouter>), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
