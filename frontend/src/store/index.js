import {action, computed, decorate, observable} from 'mobx'
class Store {
    @observable currentPage: null;
    @action setCurrentPage(payload) {
        this.currentPage = payload;
    };
    @observable currentUser: null;
    @action updateCurrentUser(payload) {
        this.currentUser = payload;
    };

    @observable currentUserId: null;
    @action setCurrentUserId(payload) {
        this.currentUserId = payload;
    };

    @observable token: null;
    @action setToken(payload) {
        this.token = payload;
    };


    @observable currentUserDatasets: null;
    @action setCurrentUserDatasets(payload) {
        this.currentUserDatasets = payload;
    };

    @observable currentUserModels: null;
    @action setCurrentUserModels(payload) {
        this.currentUserModels = payload;
    };
    @observable currentUserSubs: null;
    @action setCurrentUserSubs(payload) {
        this.currentUserSubs = payload;
    };

    @observable datasets: null;
    @action setDatasets(payload) {
        this.datasets = payload;
    };

    @observable models: null;
    @action setModels(payload) {
        this.models = payload;
    };

    // @computed get filteredCurrentPage() { //computed example
    //     return true
    // }
}

// const appStore = new Store();
// export {appStore};
// Store = decorate(Store, {
//     currentPage: observable,
//     setCurrentPage: action,
//     filteredCurrentPage: computed
// })
// export const store = new Store();
export default new Store();

