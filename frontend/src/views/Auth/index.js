import React from 'react';
import './style.scss';
import InputText from "../../components/Inputs/Text";
import Btn from "../../components/Btn";
import axios from 'axios';
import Cookies from 'universal-cookie';
import {Redirect} from "react-router";
import {observer} from "mobx-react";
import store from "../../store/index";
import {url} from "../../urls";
import {PATH_AUTH, PATH_HOME} from "../../router";

const cookies = new Cookies;

interface IState {
    authLogin: string;
    authPassword: string;
    authLoading: boolean;
    regLogin: string;
    regEmail: string;
    regFirstName: string;
    regLastName: string;
    regPassword: string;
    regPassword2: string;
    regLoading: boolean;
    redirect: boolean;
}
@observer
class Auth extends React.Component<IState> {
    state = {
    };

    constructor(props) {
        super(props);
        this.state = {
            authLogin: '',
            authPassword: '',
            authLoading: false,
            regLogin: '',
            regEmail: '',
            regFirstName: '',
            regLastName: '',
            regPassword: '',
            regPassword2: '',
            regLoading: false,
            redirect: false
        };
        this.getAuthLogin = this.getAuthLogin.bind(this);
        this.getAuthPassword = this.getAuthPassword.bind(this);
        this.getRegLogin = this.getRegLogin.bind(this);
        this.getRegEmail = this.getRegEmail.bind(this);
        this.getRegFirstName = this.getRegFirstName.bind(this);
        this.getRegLastName = this.getRegLastName.bind(this);
        this.getRegPassword = this.getRegPassword.bind(this);
        this.getRegPassword2 = this.getRegPassword2.bind(this);
        this.submitAuth = this.submitAuth.bind(this);
        this.submitRegistration = this.submitRegistration.bind(this);
    }
    UNSAFE_componentWillMount() {
        store.setCurrentPage(PATH_AUTH);
    }

    getBrowser()
    {
        let ua = navigator.userAgent;

        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/OPR/) > 0) return 'Opera';
        if (ua.search(/Edg/) > 0) return 'Edge';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        if (ua.search(/Konqueror/) > 0) return 'Konqueror';
        if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
        if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';
        if (ua.search(/Gecko/) > 0) return 'Gecko';
        //поисковый робот
        // return 'Search Bot';
    }

    submitAuth(e) {
        e.preventDefault()
        if (this.state.authLogin && this.state.authPassword) {
            const new_url = url + "auth/login/";
            let data = {
                "username" : this.state.authLogin,
                "password" : this.state.authPassword,
            }
            this.setState({authLoading: true});
            axios
                .post(new_url, data)
                .then((response) => {
                    if (this.getBrowser() === "Firefox") {
                        cookies.set("token_fdh", response.data.key, {maxAge: 60*60*6, sameSite: "lax"});
                    }
                    else {
                        cookies.set("token_fdh", response.data.key, {maxAge: 60*60*6});
                    }
                    store.setToken(response.data.key);
                    // this.props.history.push(PATH_HOME);
                    this.getDatasets();
                    this.getModels();
                    this.setState({redirect: true})
            }).catch((error) => {
                this.setState({authLoading: false});
                if (error.response) {
                    if (error.response.data.non_field_errors) {
                        let err = error.response.data.non_field_errors[0];
                        return err === "Unable to log in with provided credentials." ? alert("Введены неверные данные") : null;
                    }
                    else {
                        alert('Извините, возникла непредвиденная ошибка :(');
                    }
                }
                else {
                    alert('Извините, сервис временно недоступен :(');
                }

            })
        }
    }
    submitRegistration(e) {
        e.preventDefault()
        // if (this.state.regLogin && this.state.regEmail && this.state.regFirstName && this.state.regLastName
        //     && this.state.regPassword && this.state.regPassword2 ) {
            if (this.state.regLogin && this.state.regEmail && this.state.regPassword && this.state.regPassword2 ) {
            const new_url = url + "users/";
            let data = {
                "username": this.state.regLogin,
                "email": this.state.regEmail,
                // "first_name": this.state.regFirstName,
                // "last_name": this.state.regLastName,
                "first_name": "",
                "last_name": "",
                "password": this.state.regPassword,
                "password2": this.state.regPassword2
            }
            this.setState({regLoading: true});
            axios
                .post(new_url, data)
                .then(response => {
                    document.getElementsByClassName("registration")[0].reset();
                    this.setState({regLoading: false});
                    alert("Пользователь зарегистрирован :)");
                })
                .catch((error) => {
                    this.setState({regLoading: false});
                    // if (error.hasOwnProperty("response")) {
                    if (error.response) {
                        if (error.response.data.username) {
                            let err = error.response.data.username[0];
                            return err === "A user with that username already exists." ? alert("Пользователь с таким логином уже существует") : null;
                        }
                        else if (error.response.data.detail) {
                            let err = error.response.data.detail;
                            return err === "Пароли не совпадают" ? alert("Пароли не совпадают") : null;
                        }
                        else {
                            alert("Извините, возникла непредвиденная ошибка :(");
                        }
                    }
                    else {
                        alert("Извините, сервис временно недоступен :(");
                    }
                })
        }
    }
    getAuthLogin(value) {
        this.setState({authLogin: value})
    }
    getAuthPassword(value) {
        this.setState({authPassword: value})
    }
    getRegLogin(value) {
        this.setState({regLogin: value})
    }
    getRegEmail(value) {
        this.setState({regEmail: value})
    }
    getRegFirstName(value) {
        this.setState({regFirstName: value})
    }
    getRegLastName(value) {
        this.setState({regLastName: value})
    }
    getRegPassword(value) {
        this.setState({regPassword: value})
    }
    getRegPassword2(value) {
        this.setState({regPassword2: value})
    }
    getDatasets() { //используем чтобы при входе в приложение уже можно было искать датасеты (затем обновление происходит на странице Datasets)
        let token = store.token;
        let new_url = url + "datasets/";
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // for (let el in response.data) {
                //     let curr_res = response.data[el];
                //     new_url = url + "users/"+curr_res.user_id+"/"
                //     axios
                //         .get(new_url)
                //         .then(response => {
                //             curr_res.username = response.data.username;
                //         })
                //         .catch((error) => {
                //             curr_res.username = "";
                //         })
                //         .finally(()=> {
                //             res.push(curr_res);
                //             store.setDatasets(res);
                //         })
                // }
                store.setDatasets(response.data);
            })
            .catch((error) => {
            })
    }
    getModels() { //используем чтобы при входе в приложение уже можно было искать модели (затем обновление происходит на странице Models)
        let token = store.token;
        let new_url = url + "models/";
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // for (let el in response.data) {
                //     let curr_res = response.data[el];
                //     new_url = url + "users/"+curr_res.user_id+"/"
                //     axios
                //         .get(new_url)
                //         .then(response => {
                //             curr_res.username = response.data.username;
                //         })
                //         .catch((error) => {
                //             curr_res.username = "";
                //         })
                //         .finally(()=> {
                //             res.push(curr_res);
                //             store.setModels(res);
                //         })
                // }
                store.setModels(response.data);
            })
            .catch((error) => {
            })
    }

    render() {
        const {redirect} = this.state;
        if (redirect) {
            return (
                <Redirect to={PATH_HOME}/>
                )
        }
        else {
            return (
                <div className="auth">
                    <div className="content">
                        <span className="logo">FinDataHub</span>
                        <div className="forms">
                            <form className="authorization" onSubmit={this.submitAuth} method="POST">
                                <span className="form_name">Авторизация</span>
                                <div className="fields">
                                    <InputText
                                        text="Логин"
                                        pattern="^[a-zA-Z]+[a-zA-Z0-9_]+$"
                                        placeholder="Введите логин"
                                        sendValue={this.getAuthLogin}
                                    />
                                    <InputText
                                        text="Пароль"
                                        type="password"
                                        placeholder="Введите пароль"
                                        sendValue={this.getAuthPassword}
                                    />
                                    <Btn className="btn" text="Войти" color="light" spinner={true} loading={this.state.authLoading}/>
                                </div>
                            </form>
                            <form className="registration" onSubmit={this.submitRegistration} method="POST">
                                <span className="form_name">Регистрация</span>
                                <div className="fields">
                                    <InputText
                                        text="Логин"
                                        pattern="^[a-zA-Z]+[a-zA-Z0-9_]+$"
                                        placeholder="Введите логин"
                                        sendValue={this.getRegLogin}
                                    />
                                    <InputText
                                        text="E-mail"
                                        type="email"
                                        placeholder="Введите e-mail"
                                        sendValue={this.getRegEmail}
                                    />
                                    {/*<InputText*/}
                                    {/*    text="Имя"*/}
                                    {/*    placeholder="Введите имя"*/}
                                    {/*    sendValue={this.getRegFirstName}*/}
                                    {/*/>*/}
                                    {/*<InputText*/}
                                    {/*    text="Фамилия"*/}
                                    {/*    placeholder="Введите фамилию"*/}
                                    {/*    sendValue={this.getRegLastName}*/}
                                    {/*/>*/}
                                    <InputText
                                        text="Пароль"
                                        type="password"
                                        placeholder="Введите пароль"
                                        sendValue={this.getRegPassword}
                                    />
                                    <InputText
                                        text="Подтверждение пароля"
                                        type="password"
                                        placeholder="Введите пароль еще раз"
                                        sendValue={this.getRegPassword2}
                                    />
                                    <Btn text="Зарегистрироваться" color="light" spinner={true} loading={this.state.regLoading}/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default Auth;
