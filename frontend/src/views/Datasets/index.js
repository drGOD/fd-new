import React from 'react';
import './style.scss';
import store from '../../store'
import {PATH_AUTH, PATH_DATASETS, PATH_MODELS} from "../../router";
import {url} from "../../urls";
import axios from "axios";
import {array} from "prop-types";
import {Link} from "react-router-dom";
import Cookies from 'universal-cookie';
import {Redirect} from "react-router";

const cookies = new Cookies;

interface IState {
    loading: boolean;
    error: boolean,
    datasets: array,
    redirect: boolean
}

class Datasets extends React.Component<IState> {
    state = {

    };
    constructor() {
        super();
        this.state = {
            loading: false,
            error: false,
            datasets: [],
            redirect: false
        };
        this.getDatasets = this.getDatasets.bind(this);
    }
    componentDidMount() {
        this.onMount();
        this.getDatasets();
        this.getCurrentUserDatasets();
    }
    onMount() {
        store.setCurrentPage(PATH_DATASETS);
    }
    getDatasets() {
        let token = store.token;
        let new_url = url + "datasets/";
        this.setState({loading: true});
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // for (let el in response.data) {
                //     let curr_res = response.data[el];
                //         new_url = url + "users/"+curr_res.user_id+"/"
                //         axios
                //             .get(new_url)
                //             .then(response => {
                //                 // alert("SUCCESS");
                //                 curr_res.username = response.data.username;
                //             })
                //             .catch((error) => {
                //                 // alert("FAILURE!!!!!!HEADER!!!");
                //                 curr_res.username = "";
                //             })
                //             .finally(()=> {
                //                 res.push(curr_res);
                //                 this.setState({datasets: res});
                //                 store.setDatasets(res);
                //             })
                // }
                this.setState({datasets: response.data});
                store.setDatasets(response.data);
                this.setState({loading: false});
            })
            .catch((error) => {
                this.setState({loading: false});
                this.setState({error: true});
                if (error.response.status === 403) {
                    if (this.getBrowser() === "Firefox") {
                        cookies.remove("token_fdh", {sameSite: "lax"});
                    }
                    else {
                        cookies.remove("token_fdh");
                    }
                    store.setToken(null);
                    this.setState({redirect: true});
                }
            })
        // this.setState({datasets: [ //FAKE DATA
        //         {
        //             "id": 1,
        //             "name" : "Датасет",
        //             "user_id": 3,
        //             "desc" : "Описание датасета описание датасета описание датасета",
        //             "is_private": false,
        //             "dt": "что-то"
        //         },
        //         {
        //             "id": 2,
        //             "name" : "Второй датасет",
        //             "user_id": 3,
        //             "desc" : "Описание датасета описание датасета описание датасета",
        //             "is_private": false,
        //             "dt": "что-то"
        //         },
        //         {
        //             "id": 3,
        //             "name" : "Третий датасет",
        //             "user_id": 3,
        //             "desc" : "Описание датасета описание датасета описание датасета",
        //             "is_private": false,
        //             "dt": "что-то"
        //         }
        //     ]});
    }
    getCurrentUserDatasets() {
        let token = store.token;
        let new_url = url + "user/datasets/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                if (store.currentUserDatasets!==response.data) {
                    store.setCurrentUserDatasets(response.data);
                }
            })
            .catch((error) => {

            })
    }
    getBrowser()
    {
        let ua = navigator.userAgent;

        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/OPR/) > 0) return 'Opera';
        if (ua.search(/Edg/) > 0) return 'Edge';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        if (ua.search(/Konqueror/) > 0) return 'Konqueror';
        if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
        if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';
        if (ua.search(/Gecko/) > 0) return 'Gecko';
        //поисковый робот
        // return 'Search Bot';
    }
    render() {
        const {redirect} = this.state;
        if (redirect) {
            return (
                <Redirect to={PATH_AUTH}/>
            )
        }
        if (this.state.error) {
            return (
                <div className="datasets error">
                    <img src="/icons/cancel.svg"/>
                    <span>Извините, сервис временно недоступен</span>
                </div>
            )
        }
        else {
            return (
                <div className="datasets">
                    {
                        this.state.loading ?
                            <div className="wrap">
                                <div className="loader"></div>
                            </div>:
                    <div className="rows">

                        {
                            this.state.datasets.map((e, i) =>
                                (<div className="row" key={e.id}>
                                        <div className="top">
                                            <Link to={PATH_DATASETS + "/" + e.id} className="link">
                                                <div className="dataset_name">{e.name}</div>
                                            </Link>
                                        </div>
                                        <div className="bottom">
                                            <div className="owner">
                                                <img src="../../../icons/user_icon_dark_blue.svg"/>
                                                <div className="name">{e.username}</div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            )
                        }
                    </div>
                    }
                </div>
            );
        }
    }
}

export default Datasets;
