import React from 'react';
import './style.scss';
import store from "../../store";
import {PATH_AUTH, PATH_DATASETS, PATH_MODELS} from "../../router";
import {url} from "../../urls";
import axios from "axios";
import {array} from "prop-types";
import {Link} from "react-router-dom";
import 'codemirror/lib/codemirror.css';
import '@toast-ui/editor/dist/toastui-editor.css';
import { Editor } from '@toast-ui/react-editor';
import {Redirect} from "react-router";
import Btn from "../../components/Btn";
import Cookies from 'universal-cookie';

const cookies = new Cookies;

interface IState {
    id: number;
    prevId: number;
    model: object;
    models: array;
    history: array;
    subs: array;
    isSubscribed: boolean;
    // owner: object;
    loading: boolean;
    loadingHistory: boolean,
    // executeBtnLoading: boolean;
    historyLoading: boolean;
    executingModels: array;
    error: boolean;
    errorHistory: boolean,
    deleteLoading: boolean,
    downloadModelLoading: boolean,
    isDeleted: boolean,
    currentUserId: number,
    redirect: boolean;
    // selectedModel: number;
    // currentInformation: string;
}

class Model extends React.Component<IState> {

    editorRef = React.createRef();

    state = {

    };
    constructor() {
        super();
        this.state = {
            id: Number,
            prevId: Number,
            model: Object,
            models: [],
            history: [],
            subs: [],
            isSubscribed: false,
            // owner: Object,
            loading: false,
            // executeBtnLoading: false,
            historyLoading: false,
            loadingHistory: false,
            executingModels: [],
            error: false,
            errorHistory: false,
            deleteLoading: false,
            downloadModelLoading: false,
            isDeleted: false,
            currentUserId: Number,
            redirect: false
            // selectedModel: Number,
            // currentInformation: "description"
            // func1:this.getDataset.bind(this)
        };
        // this.showInformation = this.showInformation.bind(this);
        // this.getSelectedModel = this.getSelectedModel.bind(this);
        this.getSubs = this.getSubs.bind(this);
        this.getHistory = this.getHistory.bind(this);
        this.downloadModel = this.downloadModel.bind(this);
        this.deleteModel = this.deleteModel.bind(this);
    }
    componentDidMount() {
        store.setCurrentPage(PATH_MODELS);
        this.getModel(this.props.match.params.id);
        this.getHistory();
    }
    static getDerivedStateFromProps(props, state) {
        if (props.match.params.id !== state.id) {
            return {
                prevId: state.id,
                id: props.match.params.id
            };
        }
        else {
            return {
                prevId: props.match.params.id,
                id: props.match.params.id
            };
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.id !== this.state.prevId) {
            this.getModel(this.state.id);
            this.getHistory();
            this.setState({historyLoading: false});
            this.setState({error: false});
            this.setState({isSubscribed: false});
            this.setState({executingModels: []});
        }
    }
    getModel(id) {
        const new_url = url + "models/"+id+"/";
        this.setState({loading: true});
        axios
            .get(new_url)
            .then(response => {
                // alert("SUCCESS model by id");
                this.setState({model: response.data}, function () {
                    this.getSubs();
                })
                this.getHistory();
                this.setState({currentUserId: store.currentUser.id});
                this.setState({loading: false});
            })
            .catch((error) => {
                this.setState({loading: false});
                this.setState({error: true});
                if (error.response.status === 403) {
                    if (this.getBrowser() === "Firefox") {
                        cookies.remove("token_fdh", {sameSite: "lax"});
                    }
                    else {
                        cookies.remove("token_fdh");
                    }
                    store.setToken(null);
                    this.setState({redirect: true});
                }
            })
        // this.setState({model: {
        //         "id": 0,
        //         "name" : "model_example",
        //         "user_id": 0,
        //         "desc" : "описание описание описание описание описание описание описание" +
        //             "описание описание описание описание описание описание описание" +
        //             "описание описание описание описание описание описание описание" +
        //             "описание описание описание описание описание описание описание" +
        //             "описание описание описание описание описание описание описание" +
        //             "описание описание описание описание описание описание описание" +
        //             "описание описание описание описание описание описание описание" +
        //             "описание описание описание описание описание описание описание",
        //         "version" : 1,
        //         "price" : 200
        //     }}, function () {
        //     this.setState({loading: false});
        // })
    }
    getSubs() {
        let token = store.token;
        const new_url = url + "user/subs/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // alert("SUCCESS subs");
                this.setState({subs: response.data}, function() {
                    let subs = this.state.subs;
                    for (let key in subs) {
                        if (subs[key].sci_model_id === this.state.model.id) {
                            // alert('ID CОВПАЛИ')
                            this.setState({isSubscribed: true});
                        }
                        else {
                            // alert('НЕ СОВПАЛИ(');
                        }
                    }
                });
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!MENU!!!SUBS");
                this.setState({error: true});
            })

        // this.setState({subs: [ //FAKE DATA
        //         {
        //             "id": 1,
        //             "user_id": 3,
        //             "sci_model_id" : 5,
        //             "dt" : "smth"
        //         },
        //         {
        //             "id": 2,
        //             "user_id": 3,
        //             "sci_model_id" : 2,
        //             "dt" : "smth"
        //         }
        //     ]}, function() {
        //     let subs = this.state.subs;
        //     for (let key in subs) {
        //
        //         if (subs[key].sci_model_id === this.state.model.id) {
        //             alert('ID CОВПАЛИ')
        //             this.setState({isSubscribed: true});
        //         }
        //         else {
        //             alert('НЕ СОВПАЛИ(');
        //         }
        //     }
        // }
        // );
    }
    getHistory() {
        this.setState({loadingHistory: true});
        const new_url = url + "executions/"
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token } })
            .then(response => {
                let hist = [];
                for (let key in response.data) {
                    if (response.data[key].sci_model_id === this.state.model.id) {
                        hist[key] = response.data[key];
                        // this.setState({history: hist});
                        const new_url = url + "datasets/"+response.data[key].dataset_id+"/";
                        axios
                            .get(new_url)
                            .then(response => {
                                hist[key].dataset_name = response.data.name;
                                // let models = this.state.history;
                                // models[key].model_name = response.data.name;
                                // this.setState({history: models});
                                this.setState({history: hist});
                            })
                            .catch((error) => {
                                // alert("FAILURE!!!!!!history");
                                this.setState({errorHistory: true});
                            })
                    }
                }
                this.setState({loadingHistory: false});
            })
            .catch((error) => {
                this.setState({errorHistory: true});
                this.setState({loadingHistory: false});
            })
        // this.setState({history: [ //FAKE DATA
        //         {
        //             "id": 1,
        //             "user_id": 0,
        //             "sci_model_id": 1,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         },
        //         {
        //             "id": 2,
        //             "user_id": 0,
        //             "sci_model_id": 2,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": false
        //         },
        //         {
        //             "id": 3,
        //             "user_id": 0,
        //             "sci_model_id": 3,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         },
        //         {
        //             "id": 4,
        //             "user_id": 0,
        //             "sci_model_id": 3,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": false
        //         }
        //     ]}, function() {
        //     for (let key in this.state.history) {
        //         if (this.state.history[key].succ_status) {
        //             const new_url = url + "models/"+this.state.history[key].sci_model_id+"/";
        //             axios
        //                 .get(new_url)
        //                 .then(response => {
        //                     // alert("SUCCESS get HISTORY");
        //                     let models = this.state.history;
        //                     models[key].model_name = response.data.name;
        //                     this.setState({history: models});
        //                 })
        //                 .catch((error) => {
        //                     // alert("FAILURE!!!!!!history");
        //                 })
        //         }
        //         else {
        //             console.log('закиыдваю ид в массив...');
        //             console.log(this.state.executingModels);
        //             let arr = this.state.executingModels;
        //             arr.push(this.state.history[key].sci_model_id);
        //             this.setState({executingModels: arr});
        //             console.log(this.state.executingModels);
        //         }
        //     }
        //     if (this.state.executingModels.length!==0) {
        //         console.log('checking...');
        //         console.log(this.state.executingModels);
        //         this.checkHistory();
        //     }
        // })
    }
    async checkHistory() {
        while (store.currentPage === PATH_MODELS) {
            const new_url = url + "executions/"
            axios
                .get(new_url, { headers:
                        { Authorization: 'Token '+ store.token } })
                .then(response => {
                    let hist = [];
                    for (let key in response.data) {
                        if (response.data[key].sci_model_id === this.state.model.id) {
                            hist[key] = response.data[key];
                            // this.setState({history: hist});
                            const new_url = url + "datasets/"+response.data[key].dataset_id+"/";
                            axios
                                .get(new_url)
                                .then(response => {
                                    hist[key].dataset_name = response.data.name;
                                    // let models = this.state.history;
                                    // models[key].model_name = response.data.name;
                                    // this.setState({history: models});
                                    this.setState({history: hist});
                                })
                                .catch((error) => {
                                    // alert("FAILURE!!!!!!history");
                                    this.setState({errorHistory: true});
                                })
                        }
                    }
                })
                .catch((error) => {
                    // alert("FAILURE!!!!!!HISTORY!!!");
                    this.setState({errorHistory: true});
                })
            await new Promise(resolve => setTimeout(resolve, 10000)); //задержка на 10 секунд
        }
    }
    async checkHistory2 () {
        while (this.state.executingModels.length!==0 && store.currentPage === PATH_MODELS) {
            let executingModels = this.state.executingModels.slice(); //делаем копию массива
            for (let key in this.state.executingModels) {
                // console.log(this.state.executingModels[key]);
                const new_url = url + "executions/"+this.state.executingModels[key]+"/";
                axios
                    .get(new_url, { headers:
                            { Authorization: 'Token '+ store.token } })
                    .then(response => {
                        if (response.data.succ_status) {
                            executingModels = this.removeFromArray(executingModels, this.state.executingModels[key]);
                        }
                        // alert("SUCCESS");
                    })
                    .catch((error) => {
                        // alert("FAILURE!!!!!!HISTORY!!!");
                        this.setState({errorHistory: true});
                    })
            }
            if (this.state.executingModels.length !== executingModels.length) {
                // console.log('длины неравны');
                this.setState({executingModels: executingModels});
                this.getHistory();
            }
            await new Promise(resolve => setTimeout(resolve, 10000));
        }
        // this.setState({historyLoading: true});
    }
    formatDate(date) {
        let day = date.substr(8,2);
        let month = date.substr(5,2);
        let year = date.substr(0,4);
        return day+"."+month+"."+year
    }

    subscribe(id) {
        // alert('subscribe '+id);
        if (this.state.isSubscribed) {
            //отписываемся
            const new_url = url + "models/"+id+"/subs/";
            let token = store.token;
            axios
                .delete(new_url,
                    { headers:
                            { Authorization: 'Token '+token } })
                .then((response)=> {
                    this.setState({isSubscribed: false});
                    this.getSubscriptions();

                }).catch((error) => {
                // alert('error in unsubscribing');
            });
        }
        else {
            //подписываемся
            const new_url = url + "models/"+id+"/subs/";
            let token = store.token;
            axios
                .post(new_url,  null,{ headers:
                        { Authorization: 'Token '+token } })
                .then((response) => {
                    // alert('success subscribe');
                    this.setState({isSubscribed: true});
                    this.getSubscriptions();
                }).catch((error) => {
                // alert('error in subscribing');
            })
        }
    }
    getSubscriptions() {
        let token = store.token;
        let new_url = url + "user/subs/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                let arr = [];
                let subs = response.data;
                for (let key in subs) {
                    arr.push(subs[key].sci_model_id);
                }
                this.getModelsById(arr);
            })
            .catch((error) => {
            })
    }
    getModelsById(arr) {
        let subs_models = [];
        for (let key in arr) {
            const new_url = url + "models/"+arr[key]+"/";
            axios
                .get(new_url)
                .then(response => {
                    subs_models.push(response.data);
                    store.setCurrentUserSubs(subs_models);
                })
                .catch((error) => {
                })
                .finally(()=>{

                })
        }
    }
    deleteModel(event) {
        event.preventDefault();
        if (window.confirm('Вы действительно хотите удалить модель "'+this.state.model.name+'"?')) {
            this.setState({deleteLoading: true});
            const new_url = url + "models/"+this.state.id+"/";
            axios
                .delete(new_url, { headers:
                        { Authorization: 'Token '+ store.token } })
                .then(response => {
                    alert('Модель успешно удалена :)');
                    this.setState({deleteLoading: false});
                    this.setState({isDeleted: true});
                })
                .catch((error) => {
                    this.setState({deleteLoading: false});
                })
        }
    }
    getBrowser()
    {
        let ua = navigator.userAgent;

        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/OPR/) > 0) return 'Opera';
        if (ua.search(/Edg/) > 0) return 'Edge';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        if (ua.search(/Konqueror/) > 0) return 'Konqueror';
        if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
        if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';
        if (ua.search(/Gecko/) > 0) return 'Gecko';
        //поисковый робот
        // return 'Search Bot';
    }
    downloadExecutionFile(id, dataset_name, date) {
        const new_url = url + "executions/"+id+"/file/";

        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token }, responseType: 'blob' })
            .then(response => {
                let url = window.URL.createObjectURL(new Blob([response.data]));
                let link = document.createElement("a");
                link.href = url;
                link.download = dataset_name+"_"+date + ".csv";
                link.click();
            })
            .catch((error) => {

            })
    }
    downloadModel(event) {
        event.preventDefault();
        this.setState({downloadModelLoading: true});
        console.log('downloading model ' + this.state.model.id);
        const new_url = url + "datasets/"+this.state.model.id+"/file/";

        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token }, responseType: 'blob' })
            .then(response => {
        let url = window.URL.createObjectURL(new Blob([response.data]));
        let link = document.createElement("a");
        link.href = url;
        link.download = this.state.model.name + ".py";
        link.click();
            this.setState({downloadModelLoading: false});
        })
        .catch((error) => {
            this.setState({downloadModelLoading: false});
        })
    }

    render() {
        const {redirect} = this.state;
        if (redirect) {
            return (
                <Redirect to={PATH_AUTH}/>
            )
        }
        if (this.state.error) {
            return (
                <div className="model error">
                    <img src="/icons/cancel.svg"/>
                    <span>Извините, сервис временно недоступен</span>
                </div>
            )
        }
        else if (this.state.isDeleted) {
            return <Redirect to = {{ pathname: PATH_MODELS }} />;
        }
        else {
            return (
                <div className="model">
                    {
                        this.state.loading || this.state.loadingHistory ?
                            <div className="loader"></div> :
                            <div className="content">
                                <div className="info">
                                    <div className="name">{this.state.model.name}</div>
                                    <div className="container">
                                        <div className="billing">
                                            <img src="../../../icons/billing.svg"/>
                                            <div className="money">{parseFloat(this.state.model.price).toFixed(2)}</div>
                                            {/*<div className="money">-</div>*/}
                                        </div>
                                        <button className="subscribe"
                                                onClick={() => this.subscribe(this.state.model.id)}>
                                            {
                                                this.state.isSubscribed ?
                                                    <img src="../../../icons/star_blue.png"/> :
                                                    <img src="../../../icons/subscribe_star.svg"/>
                                            }
                                        </button>
                                        <div className="tags">
                                            {/*временно закомменчены*/}
                                            {/*<img src="../../../icons/tag_dark_blue.svg"/>*/}
                                            {/*<div className="tag">#tag</div>*/}
                                            {/*<div className="tag">#tag</div>*/}
                                        </div>
                                    </div>
                                    {
                                        (store.currentUserId===this.state.model.user_id || parseFloat(this.state.model.price)===0) &&
                                        <form className="btn_download_model" onSubmit={this.downloadModel}>
                                            <Btn text="Скачать" color="green" spinner={true}
                                                 loading={this.state.downloadModelLoading}
                                            />
                                        </form>
                                    }
                                    {
                                        this.state.model.user_id===this.state.currentUserId &&
                                        <form className="delete" onSubmit={this.deleteModel}>
                                            <Btn text="Удалить" color="red" spinner={true}
                                                 loading={this.state.deleteLoading}
                                            />
                                        </form>
                                    }
                                </div>
                                <div className="main">
                                    <div className="left">
                                        <div className="nav">
                                            <button className="nav_btn selected">Описание</button>
                                        </div>
                                        <div className="value">
                                            <div className="description">
                                            {/*{ReactHtmlParser(this.state.model.desc)}*/}

                                                <Editor
                                                    initialValue={this.state.model.desc}
                                                    previewStyle="vertical"
                                                    height="600px"
                                                    placeholder="Введите описание модели"
                                                    initialEditType="markdown"
                                                    useCommandShortcut={true}
                                                    ref={this.editorRef}
                                                    usageStatistics={false}
                                                    id="editor-markdown"
                                                    // onChange={this.markdownOnChange}
                                                    hideModeSwitch={true}
                                                    previewHighlight={false}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="history">
                                        <div className="history_header">История</div>
                                        {
                                            this.state.errorHistory ?
                                                <div className="error_history">
                                                    <img src="/icons/cancel.svg"/>
                                                    <span>Извините, история запусков временно недоступна</span>
                                                </div> :
                                        <div className="rows">
                                            {this.state.historyLoading ?
                                                <div className="row">
                                                    <div className="spinner">
                                                        <svg viewBox="0 0 50 50">
                                                            <circle
                                                                className="path"
                                                                cx="25"
                                                                cy="25"
                                                                r="15"
                                                                fill="none"
                                                                stroke-width="5"
                                                            ></circle>
                                                        </svg>
                                                    </div>
                                                </div> : null}
                                            {
                                                this
                                                    .state.history.map((e, i) =>
                                                    (
                                                        <div className="row" key={e.id}>
                                                            <Link className="link" to={PATH_DATASETS + "/" + e.dataset_id}>
                                                                <div className="row_model_name">{e.dataset_name}</div>
                                                            </Link>
                                                            <div
                                                                className="row_model_date">{this.formatDate(e.dt)}</div>
                                                            {
                                                                e.succ_status ?
                                                                    <button className="btn_download active" onClick={()=>this.downloadExecutionFile(e.id, e.dataset_name, this.formatDate(e.dt))}><img src="../../../icons/file_download_green.svg" /></button> :
                                                                    <button disabled className="btn_download disabled"><img src="../../../icons/file_download_grey.svg" /></button>
                                                            }
                                                        </div> : null))
                                            }
                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>
                    }
                </div>
            );
        }
    }
}

export default Model;
