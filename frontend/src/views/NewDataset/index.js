import React from 'react';
import './style.scss';
import store from "../../store";
import {PATH_DATASETS, PATH_MODELS, PATH_NEW_DATASET} from "../../router";
import {Redirect} from "react-router";
import File from "../../components/Inputs/File";
import InputText from "../../components/Inputs/Text";
import TextArea from "../../components/Inputs/TextArea";
import Checkbox from "../../components/Inputs/Checkbox";
import Btn from "../../components/Btn";
import {url} from "../../urls";
import axios from "axios";
import 'codemirror/lib/codemirror.css';
import '@toast-ui/editor/dist/toastui-editor.css';
import { Editor } from '@toast-ui/react-editor';

interface IState {
    id: number;
    file: object;
    datasetName: string;
    description: string;
    isPrivate: boolean,
    uploading: boolean;
    isUploaded: boolean
}

class NewDataset extends React.Component<IState>  {

    editorRef = React.createRef();

    state = {

    };
    constructor() {
        super();
        this.state = {
            id: Number,
            datasetName: "",
            description: "",
            isPrivate: false,
            uploading: false,
            isUploaded: false
        };
        this.getFile = this.getFile.bind(this);
        this.getDatasetName = this.getDatasetName.bind(this);
        this.getDescription = this.getDescription.bind(this);
        this.getPrivate = this.getPrivate.bind(this);
        this.submitUploading = this.submitUploading.bind(this);
        this.markdownOnChange = this.markdownOnChange.bind(this);
        this.getCurrentUserDatasets = this.getCurrentUserDatasets.bind(this);
    }
    componentDidMount() {
        this.onMount();
    }
    onMount() {
        store.setCurrentPage(PATH_NEW_DATASET);
    }
    getFile(value) {
        this.setState({file: value})
    }
    getDatasetName(value) {
        this.setState({datasetName: value})
    }
    getDescription(value) {
        this.setState({description: value})
    }
    getPrivate(value) {
        this.setState({isPrivate: value})
    }
    submitUploading(e) {
        e.preventDefault();
        if (this.state.file && this.state.datasetName) {
            const new_url = url + "datasets/";
            this.setState({uploading: true});

            let desc = this.editorRef.current.getInstance().getMarkdown();
            const data = new FormData();
            data.append("name", this.state.datasetName);
            data.append("desc", desc);
            data.append("desc", desc);
            data.append("is_private", this.state.isPrivate);
            data.append("file", this.state.file);

            const config = {
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "multipart/form-data",
                    "Authorization": "Token "+store.token
                }
            };

            axios
                .post(new_url, data, config)
                .then(response => {
                    this.setState({uploading: false});
                    alert("Датасет загружен");
                    this.setState({isUploaded: true, id: response.data.id})
                })
                .catch((error) => {
                    this.setState({uploading: false});
                    if (error.response) {
                        if (error.response.data.detail) {
                            alert("Извините, возникла непредвиденная ошибка :(");
                        }
                        else {
                            alert("Извините, возникла непредвиденная ошибка :(");
                        }
                    }
                    else {
                        alert("Извините, сервис временно недоступен :(");
                    }
                })
        }
    }

    markdownOnChange() {
        let markdown = this.editorRef.current.getInstance().getMarkdown();
        if (markdown !== this.state.description) {
            this.setState({description: markdown});
        }
    }

    getCurrentUserDatasets() {
        let token = store.token;
        let new_url = url + "user/datasets/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                if (store.currentUserDatasets!==response.data) {
                    store.setCurrentUserDatasets(response.data);
                }
            })
            .catch((error) => {

            })
    }
    getDatasets() {
        let token = store.token;
        let new_url = url + "datasets/";
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                for (let el in response.data) {
                    let curr_res = response.data[el];
                    new_url = url + "users/"+curr_res.user_id+"/"
                    axios
                        .get(new_url)
                        .then(response => {
                            curr_res.username = response.data.username;
                        })
                        .catch((error) => {
                            curr_res.username = "";
                        })
                        .finally(()=> {
                            res.push(curr_res);
                            store.setDatasets(res);
                        })
                }
            })
            .catch((error) => {
            })
    }

    render() {
        if (this.state.isUploaded) {
            this.getCurrentUserDatasets(); //обновляем значение массива с текущими датасетами пользователя (для левой менюшки)
            this.getDatasets(); //обновляем значение массива со всеми моделями (для Search)
            return <Redirect to = {{ pathname: PATH_DATASETS + "/" + this.state.id}} />;
        }
        else {
            return (
                <div className="new_dataset">
                    {
                        this.state.loading ?
                            <div className="loader"></div> :
                            <div className="content">
                                <div className="info">
                                    <div className="name">Добавление нового датасета</div>
                                </div>
                                <div className="main">
                                    <form className="uploading_dataset" onSubmit={this.submitUploading} method="POST">
                                        <File
                                            formats="csv"
                                            sendValue={this.getFile}
                                        />
                                        <div className="inputs_text">
                                            <InputText
                                                text="Название"
                                                pattern="^[a-zA-Zа-яА-Я]+[a-zA-Zа-яА-Я0-9_ ]+$"
                                                placeholder="Название датасета"
                                                sendValue={this.getDatasetName}
                                                style="dark"
                                            />
                                            <Checkbox
                                                text="Приватный"
                                                id="private_checkbox"
                                                sendValue={this.getPrivate}
                                            />
                                        </div>
                                        <span>Описание</span>
                                        <div className="markdowns">
                                            <div className="markdown_textarea">
                                                {/*<ReactSummernote*/}
                                                {/*    className="summernote"*/}
                                                {/*    options={{*/}
                                                {/*    lang: 'ru-RU',*/}
                                                {/*    height: 300,*/}
                                                {/*    dialogsInBody: true,*/}
                                                {/*    toolbar: [*/}
                                                {/*        ['style', ['style']],*/}
                                                {/*        ['font', ['bold', 'underline', 'clear']],*/}
                                                {/*        ['para', ['ul', 'ol', 'paragraph']],*/}
                                                {/*        ['table', ['table']],*/}
                                                {/*        ['insert', ['link', 'picture']]*/}
                                                {/*    ]*/}
                                                {/*    }}*/}
                                                {/*    onChange={this.markdownOnChange}*/}
                                                {/*/>*/}
                                                <Editor
                                                    previewStyle="vertical"
                                                    height="350px"
                                                    placeholder="Введите описание датасета"
                                                    initialEditType="wysiwyg"
                                                    useCommandShortcut={true}
                                                    ref={this.editorRef}
                                                    usageStatistics={false}
                                                    hideModeSwitch={true}
                                                    id="editor-markdown"
                                                    // onChange={this.markdownOnChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="btn_upload">
                                            <Btn text="Загрузить" color="dark" spinner={true}
                                                 loading={this.state.uploading}/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                    }
                </div>
            );
        }
    }
}

export default NewDataset;
