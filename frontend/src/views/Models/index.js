import React from 'react';
import './style.scss';
import store from '../../store'
import {PATH_AUTH, PATH_DATASETS, PATH_MODELS} from "../../router";
import {url} from "../../urls";
import axios from "axios";
import {array} from "prop-types";
import {Link} from "react-router-dom";
import Cookies from 'universal-cookie';
import {Redirect} from "react-router";

const cookies = new Cookies;

interface IState {
    loading: boolean;
    error: boolean,
    models: array,
    subscriptions: array,
    redirect: boolean;
}

class Models extends React.Component<IState> {
    state = {

    };
    constructor() {
        super();
        this.state = {
            loading: false,
            error: false,
            models: [],
            subscriptions: [],
            redirect: false
        };
        this.getModels = this.getModels.bind(this);
    }
    componentDidMount() {
        this.onMount();
        this.getModels();
        this.getSubscriptions();
        this.getCurrentUserModels();
    }
    onMount() {
        store.setCurrentPage(PATH_MODELS);
    }
    getModels() {
        let token = store.token;
        let new_url = url + "models/";
        this.setState({loading: true});
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // for (let el in response.data) {
                //     let curr_res = response.data[el];
                //     new_url = url + "users/"+curr_res.user_id+"/"
                //     axios
                //         .get(new_url)
                //         .then(response => {
                //             // console.log(response.data.username);
                //             curr_res.username = response.data.username;
                //         })
                //         .catch((error) => {
                //             curr_res.username = "";
                //         })
                //         .finally(()=> {
                //             res.push(curr_res);
                //             this.setState({models: res});
                //             store.setModels(res);
                //         })
                // }
                this.setState({models: response.data});
                store.setModels(response.data);
                this.setState({loading: false});
            })
            .catch((error) => {
                this.setState({loading: false});
                this.setState({error: true});
                if (error.response.status === 403) {
                    if (this.getBrowser() === "Firefox") {
                        cookies.remove("token_fdh", {sameSite: "lax"});
                    }
                    else {
                        cookies.remove("token_fdh");
                    }
                    store.setToken(null);
                    this.setState({redirect: true});
                }
            })
    }
    getSubscriptions() {
        let token = store.token;
        let new_url = url + "user/subs/";
        axios
                    .get(new_url, { headers:
                            { Authorization: 'Token '+token } })
                    .then(response => {
                        let arr = [];
                        let subs = response.data;
                        for (let key in subs) {
                            arr.push(subs[key].sci_model_id);
                        }
                        this.setState({subscriptions: arr});
                        this.getModelsById(arr);
                    })
                    .catch((error) => {
                    })
    }

    subscribe(id) {
        if (this.state.subscriptions.indexOf(id) !== -1) {
            //отписываемся
            const new_url = url + "models/"+id+"/subs/";
            let token = store.token;
            axios
                .delete(new_url,
                    { headers:
                            { Authorization: 'Token '+token } })
                .then((response)=> {
                    // console.log("subscriptions in subscribe");
                    // console.log(this.state.subscriptions);
                    this.getSubscriptions();
                }).catch((error) => {
                // alert('error in unsubscribing');
            });
        }
        else {
            //подписываемся
            const new_url = url + "models/"+id+"/subs/";
            let token = store.token;
            axios
                .post(new_url,  null,{ headers:
                        { Authorization: 'Token '+token } })
                .then((response) => {
                    this.getSubscriptions();
                }).catch((error) => {
                // alert('error in subscribing');
            })
        }
    }
    getModelsById(arr) {
        let subs_models = [];
        if (arr.length!==0) {
            for (let key in arr) {
                const new_url = url + "models/"+arr[key]+"/";
                axios
                    .get(new_url)
                    .then(response => {
                        // alert("SUCCESS MODELS BY ID");
                        subs_models.push(response.data);
                        // this.setState({subs_models: subs_models});
                        store.setCurrentUserSubs(subs_models);
                        // this.setState({subsLoaded: true});
                    })
                    .catch((error) => {
                    })
                    .finally(()=>{

                    })
            }
        }
        else {
            store.setCurrentUserSubs(subs_models);
        }
    }
    getCurrentUserModels() {
        let token = store.token;
        let new_url = url + "user/models/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                if (store.currentUserModels!==response.data) {
                    store.setCurrentUserModels(response.data);
                }
            })
            .catch((error) => {

            })
    }
    getBrowser()
    {
        let ua = navigator.userAgent;

        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/OPR/) > 0) return 'Opera';
        if (ua.search(/Edg/) > 0) return 'Edge';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        if (ua.search(/Konqueror/) > 0) return 'Konqueror';
        if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
        if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';
        if (ua.search(/Gecko/) > 0) return 'Gecko';
        //поисковый робот
        // return 'Search Bot';
    }

    render() {
        const {redirect} = this.state;
        if (redirect) {
            return (
                <Redirect to={PATH_AUTH}/>
            )
        }
        if (this.state.error) {
            return (
                <div className="models error">
                    <img src="/icons/cancel.svg"/>
                    <span>Извините, сервис временно недоступен</span>
                </div>
            )
        }
        else {
            return (
                <div className="models">
                    {
                        this.state.loading ?
                            <div className="wrap">
                                <div className="loader"></div>
                            </div>:
                            <div className="rows">

                                {
                                    this.state.models.map((e, i) =>
                                        (<div className="row" key={e.id}>
                                                <div className="top">
                                                    <Link to={PATH_MODELS + "/" + e.id} className="link">
                                                        <div className="model_name">{e.name}</div>
                                                    </Link>
                                                    <div className="wrap">
                                                    <div className="billing">
                                                        <img src="../../../icons/billing.svg"/>
                                                        <div className="money">{parseFloat(e.price).toFixed(2)}</div>
                                                        {/*<div className="money">-</div>*/}
                                                    </div>
                                                        <button className="subscribe"
                                                                onClick={() => this.subscribe(e.id)}>
                                                            {
                                                                this.state.subscriptions.indexOf(e.id) !== -1 ?
                                                                    <img src="../../../icons/star_blue.png"/> :
                                                                    <img src="../../../icons/subscribe_star.svg"/>
                                                            }
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className="bottom">
                                                    <div className="owner">
                                                        <img src="../../../icons/user_icon_dark_blue.svg"/>
                                                        <div className="name">{e.username}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    )
                                }
                            </div>
                    }
                </div>
            );
        }
    }
}

export default Models;
