import React from 'react';
import './style.scss';
import {array} from "prop-types";
import {url} from "../../urls";
import axios from "axios";
import store from "../../store";
import Btn from '../../components/Btn';
import Radio from '../../components/Inputs/Radio';
import {Link} from "react-router-dom";
import ReactDOM from 'react-dom';
import {PATH_AUTH, PATH_DATASETS, PATH_MODELS} from "../../router";
import 'codemirror/lib/codemirror.css';
import '@toast-ui/editor/dist/toastui-editor.css';
import { Editor } from '@toast-ui/react-editor';
import {Redirect} from "react-router";
import {observer} from "mobx-react";
import Cookies from 'universal-cookie';

const cookies = new Cookies;

interface IState {
    id: number;
    prevId: number;
    dataset: object;
    models: array;
    history: array;
    owner: object;
    loading: boolean;
    loadingOwner: boolean,
    loadingUser: boolean,
    loadingHistory: boolean,
    executeBtnLoading: boolean;
    historyLoading: boolean;
    selectedModel: number;
    currentInformation: string;
    executingModels: array;
    error: boolean;
    errorHistory: boolean,
    deleteLoading: boolean,
    downloadDatasetLoading: boolean,
    isDeleted: boolean,
    currentUserId: number,
    redirect: boolean;
}

class Dataset extends React.Component<IState> {

    editorRef = React.createRef();

    state = {

    };
    constructor() {
        super();
        this.state = {
            id: Number,
            prevId: Number,
            dataset: Object,
            models: [],
            history: [],
            owner: Object,
            loading: false,
            loadingOwner: false,
            loadingUser: false,
            loadingHistory: false,
            executeBtnLoading: false,
            historyLoading: false,
            selectedModel: Number,
            currentInformation: "description",
            executingModels: [],
            error: false,
            errorHistory: false,
            deleteLoading: false,
            downloadDatasetLoading: false,
            isDeleted: false,
            currentUserId: Number,
            redirect: false
        };
        // this.getDataset = this.getDataset.bind(this);
        this.getCurrentUser = this.getCurrentUser.bind(this);
        this.showInformation = this.showInformation.bind(this);
        this.getSelectedModel = this.getSelectedModel.bind(this);
        this.runModel = this.runModel.bind(this);
        this.getHistory = this.getHistory.bind(this);
        this.downloadDataset = this.downloadDataset.bind(this);
        this.deleteDataset = this.deleteDataset.bind(this);
    }
    componentDidMount() {
        store.setCurrentPage(PATH_DATASETS);
        this.getCurrentUser();
        this.getDataset(this.props.match.params.id);
        this.getHistory();
    }

    static getDerivedStateFromProps(props, state) {
        if (props.match.params.id !== state.id) {
            return {
                prevId: state.id,
                id: props.match.params.id
            };
        }
        else {
            return {
                prevId: props.match.params.id,
                id: props.match.params.id
            };
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.id !== this.state.prevId) {
            this.getDataset(this.state.id);
            this.getHistory();
            this.setState({historyLoading: false});
            this.setState({error: false});
            this.setState({currentInformation: "description"});
            this.setState({executingModels: []});
        }
    }
    getDataset(id) {
        let token = store.token;
        const new_url = url + "datasets/"+id+"/";
        this.setState({loading: true});

        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                // alert("SUCCESS Dataset");
                this.setState({dataset: response.data})
                this.getOwner(response.data.user_id);
                // this.getModelsForRunning();
                this.getHistory();
                this.setState({loading: false});
            })
            .catch((error) => {
                // console.log(error.response.status);
                this.setState({loading: false});
                this.setState({error: true});
                if (error.response.status === 403) {
                    if (this.getBrowser() === "Firefox") {
                        cookies.remove("token_fdh", {sameSite: "lax"});
                    }
                    else {
                        cookies.remove("token_fdh");
                    }
                    store.setToken(null);
                    this.setState({redirect: true});
                }
                // alert("FAILURE!!!!!!DATASET BY ID");

            })
        // this.setState({dataset:  //FAKE DATA
        //         {
        //             "id": id,
        //             "name" : "dataset_"+id,
        //             "user_id": 0,
        //             "desc" : "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА" +
        //                 "ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТАОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА ОПИСАНИЕ ДАТАСЕТА",
        //             "version" : 1,
        //             "price" : 1200
        //         }
        //     }, function() {
        //     this.getOwner(0);
        //     this.getModels();
        //     this.getHistory();
        //     this.setState({loading: false});
        //     }
        // );
    }
    getOwner(id) {
        const new_url = url + "users/"+id+"/";
        this.setState({loadingOwner: true});
        axios
            .get(new_url)
            .then(response => {
                // alert("SUCCESS owner");
                this.setState({owner: response.data})
                this.setState({loadingOwner: false});
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!owner BY ID");
                this.setState({error: true});
                this.setState({loadingOwner: false});
            })
        // this.setState({owner: {
        //         "id" : 0,
        //         "username": "petrovich",
        //         "first_name": "petr",
        //         "last_name": "petrovich"
        //     }})

    }
    getModelsForRunning() {
        let items = [];
        for (let i in store.currentUserModels) {
            items.push(store.currentUserModels[i]);
        }
        for (let i in store.currentUserSubs) {
            items.push(store.currentUserSubs[i]);
        }

        let unique = items.reduce((res, itm) => {
            // Test if the item is already in the new array
            let result = res.find(item => JSON.stringify(item.id) == JSON.stringify(itm.id))
            // If not lets add it
            if(!result) return res.concat(itm)
            // If it is just return what we already have
            return res
        }, [])
        this.setState({models: unique});
    }
    getModelsById(arr) { //не используется
        for (let key in arr) {
            const new_url = url + "models/"+arr[key].sci_model_id+"/";
            axios
                .get(new_url)
                .then(response => {
                    let models = this.state.models;
                    models.push(response.data);
                    this.setState({models: models});
                })
                .catch((error) => {
                    // alert("FAILURE!!!!!!MENU!!!MODELS BY ID");
                    this.setState({error: true});
                })
        }
    }
    showInformation(value) {
        if (value==="description"){
            this.setState({currentInformation: "description"});
        }
        else if (value === "current_models") {
            this.getModelsForRunning();
            this.setState({currentInformation: "current_models"})
        }
    }
    getCurrentUser() {
        const new_url = url + "user/"
        this.setState({loadingUser: true});
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token } })
            .then(response => {
                // store.updateCurrentUser(response.data);
                // store.setCurrentUserId(response.data.id);
                this.setState({currentUserId: response.data.id})
                this.setState({loadingUser: false});
            })
            .catch((error) => {
                this.setState({loadingUser: false});
            })
    }
    getSelectedModel(value) {
        this.setState({selectedModel: value})
    }
    runModel(e) {
        e.preventDefault();
        if (document.getElementsByClassName("current_models")[0].querySelector('input[type="radio"]:checked')) {
            this.setState({executeBtnLoading: true});
            let model_id = document.getElementsByClassName("current_models")[0].querySelector('input[type="radio"]:checked').value;
            const new_url = url + "models/"+model_id+"/use/";
            let token = store.token;
            axios
                .post(new_url, {
                    "dataset_id": this.props.match.params.id
                }, { headers:
                        { Authorization: 'Token '+token } })
                .then( (response) => {
                    // alert('success running');
                    if (response.data.detail === "Модель начала обработку") {
                        this.setState({historyLoading: true});
                        this.checkHistory();
                    }
                    this.setState({executeBtnLoading: false});
                }).catch((error) => {
                    // alert('error in execution');
                this.setState({executeBtnLoading: false});
                this.setState({error: true});
            })
                .finally(()=>{
                    document.getElementsByClassName("current_models")[0].reset();
                })
        }
    }
    getHistory() {
        this.setState({loadingHistory: true});
        const new_url = url + "executions/"
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token } })
            .then(response => {
                    let hist = [];
                    for (let key in response.data) {
                        if (response.data[key].dataset_id === this.state.dataset.id) {
                            hist[key] = response.data[key];
                            // this.setState({history: hist});
                            const new_url = url + "models/"+response.data[key].sci_model_id+"/";
                            axios
                                .get(new_url)
                                .then(response => {
                                    hist[key].model_name = response.data.name;
                                    // let models = this.state.history;
                                    // models[key].model_name = response.data.name;
                                    // this.setState({history: models});
                                    this.setState({history: hist});
                                })
                                .catch((error) => {
                                    // alert("FAILURE!!!!!!history");
                                    this.setState({errorHistory: true});
                                })
                        }
                    }
                this.setState({loadingHistory: false});
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!HISTORY!!!");
                this.setState({errorHistory: true});
                this.setState({loadingHistory: false});
            })
        // this.setState({history: [ //FAKE DATA
        //         {
        //             "id": 1,
        //             "user_id": 0,
        //             "sci_model_id": 1,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         },
        //         {
        //             "id": 2,
        //             "user_id": 0,
        //             "sci_model_id": 2,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": false
        //         },
        //         {
        //             "id": 3,
        //             "user_id": 0,
        //             "sci_model_id": 3,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         },
        //         {
        //             "id": 4,
        //             "user_id": 0,
        //             "sci_model_id": 3,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": false
        //         }
        //     ]}, function() {
        //     for (let key in this.state.history) {
        //         if (this.state.history[key].succ_status) {
        //             const new_url = url + "models/"+this.state.history[key].sci_model_id+"/";
        //             axios
        //                 .get(new_url)
        //                 .then(response => {
        //                     // alert("SUCCESS get HISTORY");
        //                     let models = this.state.history;
        //                     models[key].model_name = response.data.name;
        //                     this.setState({history: models});
        //                 })
        //                 .catch((error) => {
        //                     // alert("FAILURE!!!!!!history");
        //                 })
        //         }
        //         else {
        //             console.log('закиыдваю ид в массив...');
        //             console.log(this.state.executingModels);
        //             let arr = this.state.executingModels;
        //             arr.push(this.state.history[key].sci_model_id);
        //             this.setState({executingModels: arr});
        //             console.log(this.state.executingModels);
        //         }
        //     }
        //     if (this.state.executingModels.length!==0) {
        //         console.log('checking...');
        //         console.log(this.state.executingModels);
        //         this.checkHistory();
        //     }
        // })
    }
    async checkHistory() {
        while (store.currentPage === PATH_DATASETS) {
            const new_url = url + "executions/"
            axios
                .get(new_url, { headers:
                        { Authorization: 'Token '+ store.token } })
                .then(response => {
                    let hist = [];
                    for (let key in response.data) {
                        if (response.data[key].dataset_id === this.state.dataset.id) {
                            hist[key] = response.data[key];
                            // this.setState({history: hist});
                            const new_url = url + "models/"+response.data[key].sci_model_id+"/";
                            axios
                                .get(new_url)
                                .then(response => {
                                    hist[key].model_name = response.data.name;
                                    // let models = this.state.history;
                                    // models[key].model_name = response.data.name;
                                    // this.setState({history: models});
                                    this.setState({history: hist});
                                })
                                .catch((error) => {
                                    // alert("FAILURE!!!!!!history");
                                    this.setState({errorHistory: true});
                                })
                        }
                    }
                })
                .catch((error) => {
                    // alert("FAILURE!!!!!!HISTORY!!!");
                    this.setState({errorHistory: true});
                })
            await new Promise(resolve => setTimeout(resolve, 10000)); //задержка на 10 секунд
        }
    }
    async checkHistory2 () {
        while (this.state.executingModels.length!==0 && store.currentPage === PATH_DATASETS) { //пофиксить условие length!==0
            console.log('in checkHistory');
            let executingModels = this.state.executingModels.slice(); //делаем копию массива
            for (let key in this.state.executingModels) {
                console.log(this.state.executingModels[key]);
                const new_url = url + "executions/"+this.state.executingModels[key]+"/";
                axios
                    .get(new_url, { headers:
                            { Authorization: 'Token '+ store.token } })
                    .then(response => {
                        if (response.data.succ_status) {
                            executingModels = this.removeFromArray(executingModels, this.state.executingModels[key]);
                        }
                        // alert("SUCCESS");
                    })
                    .catch((error) => {
                        // alert("FAILURE!!!!!!HISTORY!!!");
                        this.setState({errorHistory: true});
                    })
            }
            if (this.state.executingModels.length !== executingModels.length) {
                // console.log('длины неравны');
                // await new Promise(resolve => setTimeout(resolve, 3000));
                this.setState({executingModels: executingModels});
                this.getHistory();
            }
            // await this.getHistory();
            await new Promise(resolve => setTimeout(resolve, 10000));
        }
        // this.setState({historyLoading: true});
    }
    formatDate(date) {
        let day = date.substr(8,2);
        let month = date.substr(5,2);
        let year = date.substr(0,4);
        return day+"."+month+"."+year
    }
    deleteDataset(event) {
        event.preventDefault();
        if (window.confirm('Вы действительно хотите удалить датасет "'+this.state.dataset.name+'"?')) {
            this.setState({deleteLoading: true});
            const new_url = url + "datasets/"+this.state.id+"/";
            axios
                .delete(new_url, { headers:
                        { Authorization: 'Token '+ store.token } })
                .then(response => {
                    alert('Датасет успешно удален :)');
                    this.setState({deleteLoading: false});
                    this.setState({isDeleted: true});
                })
                .catch((error) => {
                    this.setState({deleteLoading: false});
                })
        }
    }
    getBrowser()
    {
        let ua = navigator.userAgent;

        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/OPR/) > 0) return 'Opera';
        if (ua.search(/Edg/) > 0) return 'Edge';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        if (ua.search(/Konqueror/) > 0) return 'Konqueror';
        if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
        if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';
        if (ua.search(/Gecko/) > 0) return 'Gecko';
        //поисковый робот
        // return 'Search Bot';
    }
    downloadExecutionFile(id, model_name, date)
    {
        const new_url = url + "executions/"+id+"/file/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token }, responseType: 'blob' })
            .then(response => {
                let url = window.URL.createObjectURL(new Blob([response.data]));
                let link = document.createElement("a");
                link.href = url;
                link.download = model_name+"_"+date + ".csv";
                link.click();
            })
            .catch((error) => {

            })
    }
    downloadDataset(event) {
        event.preventDefault();
        this.setState({downloadDatasetLoading: true});
        console.log('downloading dataset ' + this.state.dataset.id);
        const new_url = url + "datasets/"+this.state.dataset.id+"/file/";

        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token }, responseType: 'blob' })
            .then(response => {
                let url = window.URL.createObjectURL(new Blob([response.data]));
                let link = document.createElement("a");
                link.href = url;
                link.download = this.state.dataset.name + ".csv";
                link.click();
                this.setState({downloadDatasetLoading: false});
            })
            .catch((error) => {
                this.setState({downloadDatasetLoading: false});
            })
    }
    render() {
        const {redirect} = this.state;
        if (redirect) {
            return (
                <Redirect to={PATH_AUTH}/>
            )
        }
        if (this.state.error) {
            return (
                <div className="dataset error">
                    <img src="/icons/cancel.svg"/>
                    <span>Извините, сервис временно недоступен</span>
                </div>
            )
        }
        else if (this.state.isDeleted) {
            return <Redirect to = {{ pathname: PATH_DATASETS }} />;
        }
        else {
            return (
                <div className="dataset">
                    {
                        // (this.state.loading && this.state.loadingOwner) ?
                        (this.state.loading || this.state.loadingOwner || this.state.loadingUser || this.state.loadingHistory) ?
                            <div className="loader"></div> :

                            <div className="content">
                                <div className="info">
                                    <div className="name">{this.state.dataset.name}</div>
                                    <div className="container">
                                        <div className="owner">
                                            <img src="../../../icons/user_icon_dark_blue.svg"/>
                                            {this.state.owner.username}
                                        </div>
                                        <div className="tags">
                                            {/*временно закомменчены*/}
                                            {/*<img src="../../../icons/tag_dark_blue.svg"/>*/}
                                            {/*<div className="tag">#tag</div>*/}
                                            {/*<div className="tag">#tag</div>*/}
                                        </div>
                                    </div>
                                    <form className="btn_download_dataset" onSubmit={this.downloadDataset}>
                                        <Btn text="Скачать" color="green" spinner={true}
                                             loading={this.state.downloadDatasetLoading}
                                        />
                                    </form>
                                    {
                                        // store.currentUser && (this.state.owner.id===this.state.currentUserId) &&
                                        (this.state.owner.id===this.state.currentUserId) &&
                                        <form className="delete" onSubmit={this.deleteDataset}>
                                            <Btn text="Удалить" color="red" spinner={true}
                                                 loading={this.state.deleteLoading}
                                            />
                                        </form>
                                    }
                                </div>
                                <div className="main">
                                    <div className="left">
                                        <div className="nav">
                                            <button
                                                className={this.state.currentInformation === "description" ? "nav_btn selected" : "nav_btn"}
                                                onClick={() => this.showInformation("description")}>Описание
                                            </button>
                                            <button
                                                className={this.state.currentInformation === "current_models" ? "nav_btn selected" : "nav_btn"}
                                                onClick={() => this.showInformation("current_models")}>Модели
                                            </button>
                                        </div>
                                        <div className="value">
                                            {
                                                this.state.currentInformation === "description" ?
                                                    <div className="description">
                                                        {/*{ReactHtmlParser(this.state.dataset.desc)}*/}

                                                        <Editor
                                                            initialValue={this.state.dataset.desc}
                                                            previewStyle="vertical"
                                                            height="600px"
                                                            initialEditType="markdown"
                                                            useCommandShortcut={true}
                                                            ref={this.editorRef}
                                                            usageStatistics={false}
                                                            id="editor-markdown"
                                                            // onChange={this.markdownOnChange}
                                                            hideModeSwitch={true}
                                                            previewHighlight={false}
                                                        />
                                                    </div> :
                                                    <form className="current_models" onSubmit={this.runModel}>
                                                        <div>
                                                            <Btn text="Запустить" spinner={true}
                                                                 loading={this.state.executeBtnLoading}/>
                                                        </div>
                                                        <div className="rows">
                                                            {
                                                                this
                                                                    .state.models.map((e, i) =>
                                                                    (<div className="row" key={e.id}>
                                                                        <Radio text={e.name}
                                                                               value={e.id}
                                                                               name="selected_model"
                                                                               id={e.id}
                                                                               sendValue={this.getSelectedModel}
                                                                        />
                                                                        <div className="desc"
                                                                             title={e.desc}>
                                                                            <Editor
                                                                                initialValue={e.desc}
                                                                                previewStyle="vertical"
                                                                                height="600px"
                                                                                initialEditType="markdown"
                                                                                useCommandShortcut={true}
                                                                                ref={this.editorRef}
                                                                                usageStatistics={false}
                                                                                id="editor-markdown"
                                                                                // onChange={this.markdownOnChange}
                                                                                hideModeSwitch={true}
                                                                                previewHighlight={false}
                                                                            />
                                                                        </div>
                                                                        <div className="billing">
                                                                            <img src="../../../icons/billing.svg"/>
                                                                            <div className="money">{parseFloat(e.price).toFixed(2)}</div>
                                                                        </div>
                                                                    </div>))
                                                            }
                                                        </div>
                                                    </form>
                                            }
                                        </div>
                                    </div>
                                    <div className="history">
                                        <div className="history_header">История</div>

                                        {
                                            this.state.errorHistory ?
                                                <div className="error_history">
                                                    <img src="/icons/cancel.svg"/>
                                                    <span>Извините, история запусков временно недоступна</span>
                                                </div> :
                                                <div className="rows">
                                                    {this.state.historyLoading ?
                                                        <div className="row">
                                                            <div className="spinner">
                                                                <svg viewBox="0 0 50 50">
                                                                    <circle
                                                                        className="path"
                                                                        cx="25"
                                                                        cy="25"
                                                                        r="15"
                                                                        fill="none"
                                                                        stroke-width="5"
                                                                    ></circle>
                                                                </svg>
                                                            </div>
                                                        </div> : null}
                                                    {
                                                        this
                                                            .state.history.map((e, i) =>
                                                            (
                                                                <div className="row" key={e.id}>
                                                                    <Link className="link"
                                                                          to={PATH_MODELS + "/" + e.sci_model_id}>
                                                                        <div
                                                                            className="row_model_name">{e.model_name}</div>
                                                                    </Link>
                                                                    <div
                                                                        className="row_model_date">{this.formatDate(e.dt)}</div>
                                                                    {
                                                                        e.succ_status ?
                                                                            <button className="btn_download active" onClick={()=>this.downloadExecutionFile(e.id, e.model_name, this.formatDate(e.dt))}><img src="../../../icons/file_download_green.svg" /></button> :
                                                                            <button disabled className="btn_download disabled"><img src="../../../icons/file_download_grey.svg" /></button>
                                                                    }

                                                                </div>))
                                                    }
                                                </div>
                                        }
                                    </div>
                                </div>
                            </div>
                    }
                </div>
            );
        }
    }
}

export default observer(Dataset);
