import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import {
    PATH_DATASETS, PATH_NEW_DATASET, PATH_DATASET,
    PATH_MODELS, PATH_NEW_MODEL, PATH_MODEL,
    PATH_PROFILE, PATH_HOME, PATH_AUTH
} from "../../router";
import Header from '../../components/Header/index';
import Menu from '../../components/Menu';
import Profile from "../Profile";
import Datasets from "../Datasets";
import NewDataset from "../NewDataset";
import Dataset from "../Dataset";
import Models from "../Models";
import NewModel from "../NewModel";
import Model from "../Model";

import './style.scss';
import {PrivateRoute} from "../../components/helpers/PrivateRoute";
import store from "../../store";

class Main extends React.Component {
    UNSAFE_componentWillMount() {
        store.setCurrentPage(PATH_HOME);
    }
    render() {
        const { history } = this.props

        return (
            <div className="main_page">
                <span>Добро пожаловать в FinDataHub!</span>
                <img src="../../../icons/hello.svg" />
            </div>
        );
    }
}

export default Main;
