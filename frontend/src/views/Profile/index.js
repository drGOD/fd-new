import React from 'react';
import './style.scss';
import store from "../../store";
import {PATH_AUTH, PATH_DATASETS, PATH_PROFILE, PATH_MODELS} from "../../router";
import InputShowText from "./components/InputShowText";
import Btn from "../../components/Btn";
import Cookies from 'universal-cookie';
import {Redirect} from "react-router";
import InputText from "../../components/Inputs/Text";
import {url} from "../../urls";
import axios from "axios";
import {array} from "prop-types";
import {Link} from "react-router-dom";

const cookies = new Cookies;

interface IState {
    redirect: boolean;
    loading: boolean;
    loadingHistory: boolean,
    changeFirstName: boolean;
    changeLastName: boolean;
    defaultName: string;
    newFirstName: string;
    newLastName: string;
    saveLoading: boolean;
    userInfo: object;
    history: array;
    historyLoading: boolean;
    executingModels: array;
    error: boolean;
    errorHistory: boolean,
    deleteLoading: boolean
}

class Profile extends React.Component<IState> {
    state = {
    };

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            loading: false,
            loadingHistory: false,
            changeFirstName: false,
            changeLastName: false,
            newFirstName: String,
            newLastName: String,
            saveLoading: false,
            defaultName: "James",
            userInfo: Object,
            history: [],
            historyLoading: false,
            executingModels: [],
            error: false,
            errorHistory: false,
            deleteLoading: false
        };
        this.exit = this.exit.bind(this);
        this.changeVisibility = this.changeVisibility.bind(this);
        this.getNewFirstName = this.getNewFirstName.bind(this);
        this.getNewLastName = this.getNewLastName.bind(this);
        this.getUserInfo = this.getUserInfo.bind(this);
        this.updateUserInfo = this.updateUserInfo.bind(this);
        this.getHistory = this.getHistory.bind(this);
        this.getHistory2 = this.getHistory2.bind(this);
        this.deleteAccount = this.deleteAccount.bind(this);
    }
    componentDidMount() {
        this.onMount();
        this.getUserInfo();
        this.getHistory();
    }
    onMount() {
        store.setCurrentPage(PATH_PROFILE);
    }
    getUserInfo() {
        this.setState({loading: true});
        const new_url = url + "user/"
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token } })
            .then(response => {
                this.setState({userInfo: response.data});
                this.setState({loading: false});
                // alert("SUCCESS userInfo");
            })
            .catch((error) => {
                this.setState({loading: false});
                this.setState({error: true});
                if (error.response.status === 403) {
                    if (this.getBrowser() === "Firefox") {
                        cookies.remove("token_fdh", {sameSite: "lax"});
                    }
                    else {
                        cookies.remove("token_fdh");
                    }
                    store.setToken(null);
                    this.setState({redirect: true});
                }
            })
            .finally(() =>{

            })
        // this.setState({userInfo: {
        //         "id" : 0,
        //         "username": "Ryan",
        //         "first_name": "kekeke",
        //         "last_name": "vdf"
        //     }})
    }
    getBrowser()
    {
        let ua = navigator.userAgent;

        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/OPR/) > 0) return 'Opera';
        if (ua.search(/Edg/) > 0) return 'Edge';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        if (ua.search(/Konqueror/) > 0) return 'Konqueror';
        if (ua.search(/Iceweasel/) > 0) return 'Debian Iceweasel';
        if (ua.search(/SeaMonkey/) > 0) return 'SeaMonkey';
        if (ua.search(/Gecko/) > 0) return 'Gecko';
        //поисковый робот
        // return 'Search Bot';
    }
    exit(e) {
        e.preventDefault();
        if (this.getBrowser() === "Firefox") {
            cookies.remove("token_fdh", {sameSite: "lax"});
        }
        else {
            cookies.remove("token_fdh");
        }
        store.setToken(null);
        this.setState({redirect: true});
    }
    changeVisibility(value) {
        if (value==="first_name") {
            this.setState({newFirstName: this.state.userInfo.first_name});
            this.setState({changeFirstName: true});
        }
        else if (value==="last_name") {
            this.setState({newLastName: this.state.userInfo.last_name});
            this.setState({changeLastName: true});
        }
    }
    getNewFirstName(value) {
        this.setState({newFirstName: value});
    }
    getNewLastName(value) {
        this.setState({newLastName: value});
    }
    updateUserInfo(e) {
        e.preventDefault();
        this.setState({saveLoading: true});
        // alert(this.state.newFirstName);
        let new_url = url + "user/";
        let data = {
            "first_name": this.state.newFirstName,
            "last_name" : this.state.newLastName
        }
        axios
            .put(new_url, data,{ headers:
                    { Authorization: 'Token '+ store.token } })
            .then(response => {
                // alert("SUCCESS EDITING INFO");
                new_url = url + "user/"
                axios
                    .get(new_url, { headers:
                            { Authorization: 'Token '+ store.token } })
                    .then(response => {
                        store.updateCurrentUser(response.data);
                        this.setState({userInfo: response.data});
                        this.setState({saveLoading: false});
                        this.setState({changeFirstName: false});
                        this.setState({changeLastName: false});
                        // alert("SUCCESS userInfo");
                    })
                    .catch((error) => {
                        // alert("FAILURE!!!!!! userInfo");
                        this.setState({saveLoading: false});
                        this.setState({changeFirstName: false});
                        this.setState({changeLastName: false});
                    })
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!EDITING INFO");
                this.setState({saveLoading: false});
                this.setState({changeFirstName: false});
                this.setState({changeLastName: false});
            })
    }
    getHistory() {
        this.setState({loadingHistory: true});
        const new_url = url + "executions/"
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token } })
            .then(response => {
                let hist = [];
                for (let key in response.data) {
                    let response_data = response.data;
                    hist[key] = response.data[key];
                    let new_url = url + "datasets/"+response.data[key].dataset_id+"/";
                    axios
                        .get(new_url)
                        .then(response => {
                                hist[key].dataset_name = response.data.name;
                                // let models = this.state.history;
                                // models[key].model_name = response.data.name;
                                // this.setState({history: models});
                            // this.setState({history: hist});
                            new_url = url + "models/"+response_data[key].sci_model_id+"/";
                            axios
                                .get(new_url)
                                .then(response => {
                                    hist[key].model_name = response.data.name;
                                    // let models = this.state.history;
                                    // models[key].model_name = response.data.name;
                                    // this.setState({history: models});
                                    this.setState({history: hist});
                                })
                                .catch((error) => {
                                    // alert("FAILURE!!!!!!history");
                                    this.setState({errorHistory: true});
                                })
                        })
                        .catch((error) => {
                                // alert("FAILURE!!!!!!history");
                            this.setState({errorHistory: true});
                        })
                }
                this.setState({loadingHistory: false});
            })
            .catch((error) => {
                this.setState({errorHistory: true});
                this.setState({loadingHistory: false});
                // alert("FAILURE!!!!!!HISTORY!!!");
            })
    }
    getHistory2() { //временно не изпользуется
        const new_url = url + "executions/"
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token } })
            .then(response => {
                this.setState({history: response.data}, function() {
                    for (let key in this.state.history) {
                        if (this.state.history[key].succ_status) {
                            const new_url = url + "models/"+this.state.history[key].sci_model_id+"/";
                            axios
                                .get(new_url)
                                .then(response => {
                                    // alert("SUCCESS get HISTORY");
                                    let models = this.state.history;
                                    models[key].model_name = response.data.name;
                                    this.setState({history: models});
                                })
                                .catch((error) => {
                                    // alert("FAILURE!!!!!!history");
                                    this.setState({errorHistory: true});
                                })
                        }
                        else {
                            // console.log('закиыдваю ид в массив...');
                            // console.log(this.state.executingModels);
                            let arr = this.state.executingModels;
                            arr.push(this.state.history[key].sci_model_id);
                            this.setState({executingModels: arr});
                            // console.log(this.state.executingModels);
                        }
                    }
                    if (this.state.executingModels.length!==0) {
                        // console.log('checking...');
                        // console.log(this.state.executingModels);
                        this.checkHistory();
                    }
                })
                // alert("SUCCESS");
            })
            .catch((error) => {
                // alert("FAILURE!!!!!!HISTORY!!!");
                this.setState({errorHistory: true});
            })
        // this.setState({history: [ // FAKE DATA
        //         {
        //             "id": 1,
        //             "user_id": 0,
        //             "sci_model_id": 1,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         },
        //         {
        //             "id": 2,
        //             "user_id": 0,
        //             "sci_model_id": 2,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         },
        //         {
        //             "id": 3,
        //             "user_id": 0,
        //             "sci_model_id": 3,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         },
        //         {
        //             "id": 3,
        //             "user_id": 0,
        //             "sci_model_id": 3,
        //             "dt": "2020-05-25T10:33:12.308992Z",
        //             "dataset_id": 0,
        //             "err_log": "kek",
        //             "succ_status": true
        //         }
        //     ]}, function() {
        //     for (let key in this.state.history) {
        //         if (this.state.history[key].succ_status) {
        //             const new_url = url + "models/"+this.state.history[key].sci_model_id+"/";
        //             axios
        //                 .get(new_url)
        //                 .then(response => {
        //                     // alert("SUCCESS get HISTORY");
        //                     let models = this.state.history;
        //                     models[key].model_name = response.data.name;
        //                     this.setState({history: models});
        //                 })
        //                 .catch((error) => {
        //                     // alert("FAILURE!!!!!!history");
        //                 })
        //         }
        //         else {
        //             console.log('закиыдваю ид в массив...');
        //             console.log(this.state.executingModels);
        //             let arr = this.state.executingModels;
        //             arr.push(this.state.history[key].sci_model_id);
        //             this.setState({executingModels: arr});
        //             console.log(this.state.executingModels);
        //         }
        //     }
        //     if (this.state.executingModels.length!==0) {
        //         console.log('checking...');
        //         console.log(this.state.executingModels);
        //         this.checkHistory();
        //     }
        // })
    }
    async checkHistory () {
        while (this.state.executingModels.length!==0 && store.currentPage === PATH_PROFILE) {
            let executingModels = this.state.executingModels.slice(); //делаем копию массива
            for (let key in this.state.executingModels) {
                // console.log(this.state.executingModels[key]);
                const new_url = url + "executions/"+this.state.executingModels[key]+"/";
                axios
                    .get(new_url, { headers:
                            { Authorization: 'Token '+ store.token } })
                    .then(response => {
                        if (response.data.succ_status) {
                            executingModels = this.removeFromArray(executingModels, this.state.executingModels[key]);
                        }
                        // alert("SUCCESS");
                    })
                    .catch((error) => {
                        // alert("FAILURE!!!!!!HISTORY!!!");
                        this.setState({errorHistory: true});
                    })
            }
            if (this.state.executingModels.length !== executingModels.length) {
                // console.log('длины неравны');
                // await new Promise(resolve => setTimeout(resolve, 3000));
                this.setState({executingModels: executingModels});
                this.getHistory();
            }
            // await this.getHistory();
            await new Promise(resolve => setTimeout(resolve, 10000));
        }
        // this.setState({historyLoading: true});
    }
    removeFromArray(arr) {
        let what, a = arguments, L = a.length, ax;
        while (L > 1 && arr.length) {
            what = a[--L];
            while ((ax= arr.indexOf(what)) !== -1) {
                arr.splice(ax, 1);
            }
        }
        return arr;
    }
    formatDate(date) {
        let day = date.substr(8,2);
        let month = date.substr(5,2);
        let year = date.substr(0,4);
        return day+"."+month+"."+year
    }
    deleteAccount(event) {
        event.preventDefault();
        if (window.confirm('Вы действительно хотите удалить аккаунт?')) {
            this.setState({deleteLoading: true});

            const new_url = url + "user/";
            axios
                .delete(new_url, { headers:
                        { Authorization: 'Token '+ store.token } })
                .then(response => {
                    alert('Аккаунт успешно удален :)');
                    this.setState({deleteLoading: false});
                    this.exit(event);
                })
                .catch((error) => {
                    alert('Что-то пошло не так...');
                    this.setState({deleteLoading: false});
                })
        }
    }
    downloadExecutionFile(id, dataset_name, model_name, date) {
        const new_url = url + "executions/"+id+"/file/";

        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+ store.token }, responseType: 'blob' })
            .then(response => {
                let url = window.URL.createObjectURL(new Blob([response.data]));
                let link = document.createElement("a");
                link.href = url;
                link.download = dataset_name+"_"+model_name+"_"+date + ".csv";
                link.click();
            })
            .catch((error) => {

            })
    }

    render() {
        const {redirect} = this.state;
        if (redirect) {
            return (
                <Redirect to={PATH_AUTH}/>
            )
        }
        else {
            if (this.state.error) {
                return (
                    <div className="profile error">
                        <img src="/icons/cancel.svg"/>
                        <span>Извините, сервис временно недоступен</span>
                    </div>
                )
            }
            else {
                return (
                    <div className="profile">
                        {
                            this.state.loading || this.state.loadingHistory ?
                                <div className="loader"></div> :
                                <div className="content">
                                    <div className="inputs">
                                        <form className="info" onSubmit={this.updateUserInfo}>
                                            <InputShowText title="Логин" value={this.state.userInfo.username}/>
                                            {
                                                this.state.changeFirstName ?
                                                    <>
                                                        <InputText text="Имя"
                                                                   defaultValue={this.state.userInfo.first_name}
                                                                   style="dark" sendValue={this.getNewFirstName}/>
                                                        <Btn text="Сохранить" spinner={true}
                                                             loading={this.state.saveLoading}/>
                                                    </>
                                                    :
                                                    <InputShowText title="Имя" value={this.state.userInfo.first_name}
                                                                   editable={true}
                                                                   sendValue={() => this.changeVisibility("first_name")}
                                                    />
                                            }
                                            {
                                                this.state.changeLastName ?
                                                    <>
                                                        <InputText text="Фамилия"
                                                                   defaultValue={this.state.userInfo.last_name}
                                                                   style="dark" sendValue={this.getNewLastName}/>
                                                        <Btn text="Сохранить" spinner={true}
                                                             loading={this.state.saveLoading}/>
                                                    </>
                                                    :
                                                    <InputShowText title="Фамилия" value={this.state.userInfo.last_name}
                                                                   editable={true}
                                                                   sendValue={() => this.changeVisibility("last_name")}
                                                    />
                                            }
                                        </form>
                                        <div className="btns">
                                            <form onSubmit={this.deleteAccount} className="delete">
                                                <Btn text="Удалить аккаунт" color="red" spinner={true}
                                                     loading={this.state.deleteLoading} />
                                            </form>
                                            <form onSubmit={this.exit} className="exit">
                                                <Btn text="Выйти"/>
                                            </form>
                                        </div>
                                    </div>
                                    <div className="billing">
                                        <div>
                                            <img src="../../../icons/billing.svg"/>
                                            -
                                        </div>
                                    </div>
                                    <div className="history">
                                        <div className="history_header">История</div>
                                        {
                                            this.state.errorHistory ?
                                                <div className="error_history">
                                                    <img src="/icons/cancel.svg"/>
                                                    <span>Извините, история запусков временно недоступна</span>
                                                </div> :
                                                <div className="rows">
                                                    {this.state.historyLoading ?
                                                        <div className="row">
                                                            <div className="spinner">
                                                                <svg viewBox="0 0 50 50">
                                                                    <circle
                                                                        className="path"
                                                                        cx="25"
                                                                        cy="25"
                                                                        r="15"
                                                                        fill="none"
                                                                        stroke-width="5"
                                                                    ></circle>
                                                                </svg>
                                                            </div>
                                                        </div> : null}
                                                    { this.state.history.length !==0 && <div className="history_titles">
                                                        <div className="hist_dataset">Датасет</div>
                                                        <div className="hist_model">Модель</div>
                                                        <div className="hist_date">Дата</div>
                                                    </div>}
                                                    {
                                                        this
                                                            .state.history.map((e, i) =>
                                                            (
                                                                <div className="row" key={e.id}>
                                                                    <Link className="link" to={PATH_DATASETS + "/" + e.dataset_id}>
                                                                        <div className="row_dataset_name">{e.dataset_name}</div>
                                                                    </Link>
                                                                    <Link className="link" to={PATH_MODELS + "/" + e.sci_model_id}>
                                                                        <div className="row_model_name">{e.model_name}</div>
                                                                    </Link>
                                                                    <div
                                                                        className="row_model_date">{this.formatDate(e.dt)}</div>
                                                                    {
                                                                        e.succ_status ?
                                                                            <button className="btn_download active" onClick={()=>this.downloadExecutionFile(e.id, e.dataset_name, e.model_name,this.formatDate(e.dt))}><img src="../../../icons/file_download_green.svg" /></button> :
                                                                            <button disabled className="btn_download disabled"><img src="../../../icons/file_download_grey.svg" /></button>
                                                                    }
                                                                </div> : null))
                                                    }
                                                </div>
                                        }

                                    </div>
                                </div>
                        }
                    </div>
                );
            }
        }
    }
}

export default Profile;
