import React from 'react';
import './style.scss';
import store from "../../../../store";
interface Props {
    title?: String, // заголовок
    value?: String, // значение инпута
    editable: Boolean, // возможность редактировать поле
}

class InputShowText extends React.Component<Props> {
    static defaultProps = {
        editable: false
    };
    constructor(props) {
        super(props);
        this.changeInput = this.changeInput.bind(this);
    }
    componentDidMount() {
        // this.onMount();
    }
    onMount() {
        // store.setCurrentPage(PATH_PROFILE);
    }
    changeInput() {
        this.props.sendValue(true);
    }

    render() {
        return (
            <div className="input_show_text">
                <div className="title">
                    <div>{this.props.title}</div>
                    {
                        this.props.editable ?  <img onClick={this.changeInput} src="../../../../../icons/pencil.svg" /> : null
                    }
                </div>
                <div className="value">{this.props.value}</div>
            </div>
        );
    }
}

export default InputShowText;
