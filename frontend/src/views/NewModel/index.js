import React from 'react';
import './style.scss';
import store from "../../store";
import {PATH_MODELS, PATH_NEW_MODEL} from "../../router";
import InputText from "../../components/Inputs/Text";
import File from "../../components/Inputs/File";
import TextArea from "../../components/Inputs/TextArea";
import Btn from "../../components/Btn";
import {url} from "../../urls";
import axios from "axios";
import {Redirect} from "react-router";
import 'codemirror/lib/codemirror.css';
import '@toast-ui/editor/dist/toastui-editor.css';
import { Editor } from '@toast-ui/react-editor';

interface IState {
    id: number;
    loading: boolean;
    file: object,
    modelName: string;
    version: number;
    price: number;
    description: string,
    uploading: boolean,
    isUploaded: boolean
}

class NewModel extends React.Component<IState> {

    editorRef = React.createRef();

    state = {

    };
    constructor() {
        super();
        this.state = {
            id: Number,
            loading: false,
            modelName: "",
            version: Number,
            price: Number,
            description: "",
            uploading: false,
            isUploaded: false
        };
        this.getFile = this.getFile.bind(this);
        this.getModelName = this.getModelName.bind(this);
        this.getVersion = this.getVersion.bind(this);
        this.getPrice = this.getPrice.bind(this);
        this.getDescription = this.getDescription.bind(this);
        this.submitUploading = this.submitUploading.bind(this);
        this.markdownOnChange = this.markdownOnChange.bind(this);
        this.getCurrentUserModels = this.getCurrentUserModels.bind(this);
    }
    componentDidMount() {
        this.onMount();
    }
    onMount() {
        store.setCurrentPage(PATH_NEW_MODEL);
    }
    getFile(value) {
        this.setState({file: value})
    }
    getModelName(value) {
        this.setState({modelName: value})
    }
    getVersion(value) {
        this.setState({version: value})
    }
    getPrice(value) {
        this.setState({price: value})
    }
    getDescription(value) {
        this.setState({description: value})
    }
    submitUploading(e) {
        e.preventDefault();
        if (this.state.file && this.state.modelName) {
            const new_url = url + "models/";
            this.setState({uploading: true});
            let desc = this.editorRef.current.getInstance().getMarkdown();
            const data = new FormData();
            data.append("name", this.state.modelName);
            data.append("desc", desc);
            data.append("version", this.state.version);
            data.append("price", this.state.price);
            data.append("file", this.state.file);

            const config = {
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "multipart/form-data",
                    "Authorization": "Token "+store.token
                }
            };

            axios
                .post(new_url, data, config)
                .then(response => {
                    this.setState({uploading: false});
                    alert("Модель загружена");
                    this.setState({isUploaded: true, id: response.data.id});


                })
                .catch((error) => {
                    this.setState({uploading: false});
                    if (error.response) {
                       if (error.response.data.detail) {
                           alert("Извините, возникла непредвиденная ошибка :(");
                        }
                        else {
                            alert("Извините, возникла непредвиденная ошибка :(");
                        }
                    }
                    else {
                        alert("Извините, сервис временно недоступен :(");
                    }
                })
        }
    }
    markdownOnChange(value) {
        this.setState({description: value});
    }

    getCurrentUserModels() {
        let token = store.token;
        let new_url = url + "user/models/";
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                if (store.currentUserModels!==response.data) {
                    store.setCurrentUserModels(response.data);
                }
            })
            .catch((error) => {

            })
    }
    getModels() {
        let token = store.token;
        let new_url = url + "models/";
        let res = [];
        axios
            .get(new_url, { headers:
                    { Authorization: 'Token '+token } })
            .then(response => {
                for (let el in response.data) {
                    let curr_res = response.data[el];
                    new_url = url + "users/"+curr_res.user_id+"/"
                    axios
                        .get(new_url)
                        .then(response => {
                            curr_res.username = response.data.username;
                        })
                        .catch((error) => {
                            curr_res.username = "";
                        })
                        .finally(()=> {
                            res.push(curr_res);
                            store.setModels(res);
                        })
                }
            })
            .catch((error) => {
            })
    }

    render() {
        if (this.state.isUploaded) {
            this.getCurrentUserModels(); //обновляем значение массива с текущими моделями пользователя (для левой менюшки)
            this.getModels(); //обновляем значение массива со всеми моделями (для Search)
            return <Redirect to = {{ pathname: PATH_MODELS+"/"+ this.state.id}} />;
        }
        else {
            return (
                <div className="new_model">
                    {
                        this.state.loading ?
                            <div className="loader"></div> :
                            <div className="content">
                                <div className="info">
                                    <div className="name">Добавление новой модели</div>
                                </div>
                                <div className="main">
                                    <form className="uploading_model" onSubmit={this.submitUploading} method="POST">
                                        <File
                                            formats="py, zip"
                                            sendValue={this.getFile}
                                        />
                                        <div className="inputs_text">
                                            <InputText
                                                text="Название"
                                                pattern="^[a-zA-Zа-яА-Я]+[a-zA-Zа-яА-Я0-9_ ]+$"
                                                placeholder="Название модели"
                                                sendValue={this.getModelName}
                                                style="dark"
                                            />
                                            <InputText
                                                text="Версия"
                                                placeholder="Номер"
                                                type="number"
                                                required={false}
                                                defaultValue={1}
                                                sendValue={this.getVersion}
                                                style="dark"
                                                min={0}
                                                id="version"
                                                width={80}
                                            />
                                            <div className="container_price">
                                                <InputText
                                                    text="Стоимость"
                                                    placeholder="Стоимость"
                                                    type="number"
                                                    required={false}
                                                    defaultValue={0}
                                                    sendValue={this.getPrice}
                                                    style="dark"
                                                    min={0}
                                                    step="any"
                                                    id="price"
                                                    width={150}
                                                />
                                                <img className="billing" src="../../../icons/billing.svg"/>
                                            </div>
                                        </div>
                                        <span>Описание</span>
                                        <div className="markdowns">
                                            <div className="markdown_textarea">
                                                {/*<ReactSummernote*/}
                                                {/*    options={{*/}
                                                {/*    lang: 'ru-RU',*/}
                                                {/*    height: 300,*/}
                                                {/*    dialogsInBody: true,*/}
                                                {/*    toolbar: [*/}
                                                {/*        ['style', ['style']],*/}
                                                {/*        ['font', ['bold', 'underline', 'clear']],*/}
                                                {/*        ['para', ['ul', 'ol', 'paragraph']],*/}
                                                {/*        ['table', ['table']],*/}
                                                {/*        ['insert', ['link', 'picture']]*/}
                                                {/*    ]*/}
                                                {/*    }}*/}
                                                {/*    onChange={this.markdownOnChange}*/}
                                                {/*/>*/}

                                                <Editor
                                                    previewStyle="vertical"
                                                    height="350px"
                                                    placeholder="Введите описание модели"
                                                    initialEditType="wysiwyg"
                                                    useCommandShortcut={true}
                                                    ref={this.editorRef}
                                                    usageStatistics={false}
                                                    id="editor-markdown"
                                                    hideModeSwitch={true}
                                                    // onChange={this.markdownOnChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="btn_upload">
                                            <Btn text="Загрузить" color="dark" spinner={true}
                                                 loading={this.state.uploading}/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                    }
                </div>
            );
        }
    }
}

export default NewModel;
