import numpy as np
import pandas as pd


def main(data):
    dffull = data

    dffull.dropna(subset=['risk'], axis='rows', inplace=True)

    # "категории" для значений риска
    riskbins = np.linspace(0.00, 16.50, 100)
    # "категории" для значений доходности
    incomebins = np.linspace(-0.25, 0.15, 100)
    # биннинг датасета без ограничений
    dffull['riskbins'] = pd.cut(dffull['risk'], bins=riskbins)
    dffull['incomebins'] = pd.cut(dffull['income'], bins=incomebins)

    dfeffective = dffull

    dfeffective.incomebins = dfeffective.incomebins.astype('object')
    dfeffective = dfeffective.loc[dfeffective.groupby('incomebins')[
        'risk'].idxmin()]

    return dfeffective
