from __future__ import print_function
import os
import sys
import threading
from time import sleep
import _thread as thread

try:  # use code that works the same in Python 2 and 3
    range, _print = xrange, print

    def print(*args, **kwargs):
        flush = kwargs.pop('flush', False)
        _print(*args, **kwargs)
        if flush:
            kwargs.get('file', sys.stdout).flush()
except NameError:
    pass


class HiddenPrints:
    '''
    Class for hiding system output
    Usage:

        with HiddenPrints():
            ...
    '''

    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout


def _cdquit(fn_name):
    print(f'{fn_name} took too long', file=sys.stderr)
    sys.stderr.flush()
    sys.stdout.flush()
    os._exit(1)


def exit_after(s):
    '''
    Use as decorator to exit process if 
    function takes longer than s seconds

    Args:
        s: seconds

    Returns:
        decorated function
    '''
    def outer(fn):
        def inner(*args, **kwargs):
            timer = threading.Timer(s, _cdquit, args=[fn.__name__])
            timer.start()
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result
        return inner
    return outer
