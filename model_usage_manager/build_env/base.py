"""
Script for executing sci_model inside container
- import model
- load dataset
- execute model with dataset
- save result as csv file

Usage: python base.py <model_path> <dataset_path> <result_path>
"""

import sys
import importlib.util
import pandas as pd
import zipfile
import os
from utils import HiddenPrints, exit_after


# reading command line arguments
type = sys.argv[1].split(".")[-1]
if type == "zip":
    zp = zipfile.ZipFile(sys.argv[1])
    zp.extractall()
    folder = sys.argv[1].split("/")[-1].split(".")[0]
    MODEL_PATH = './' + folder + "/model.py"
else:
    MODEL_PATH = sys.argv[1]

DATASET_PATH = sys.argv[2]
RES_PATH = sys.argv[3]
MODEL_TIMEOUT = sys.argv[4]

# hide traceback
# sys.tracebacklimit = 0


@exit_after(int(MODEL_TIMEOUT))
def exec_model(func, data):
    with HiddenPrints():
        # executing model
        result = pd.DataFrame(func(data))
    return result


# importing model
spec = importlib.util.spec_from_file_location(
    "model.main", MODEL_PATH)

module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)

# importing dataset
data = pd.read_csv(DATASET_PATH, delimiter=';')

result = exec_model(module.main, data)

result.to_csv(RES_PATH)
