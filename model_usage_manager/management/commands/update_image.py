from django.core.management.base import BaseCommand
from django.utils import timezone
from data_hub.settings.local import BASE_IMAGE_NAME
from model_usage_manager.docker_api import build_base_image


class Command(BaseCommand):
    help = 'Updates or builds base docker image for sci_model usage.'

    def handle(self, *args, **kwargs):
        self.stdout.write("\tUpdating image...")
        try:
            build_base_image()
        except Exception as ex:
            self.stdout.write(self.style.ERROR(f"\tError occured:\n{ex}"))
        else:
            self.stdout.write(self.style.SUCCESS(
                f"\tImage {BASE_IMAGE_NAME} successfully built!"))
