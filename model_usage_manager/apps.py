from django.apps import AppConfig


class ModelUsageManagerConfig(AppConfig):
    name = 'model_usage_manager'
