from data_hub.settings.local import (
    PROJ_ROOT,
    BASE_IMAGE_NAME,
    WORKDIR,
    MOUNT_SOURCE_UPL,
    MOUNT_TARGET_UPL,
    MOUNT_SOURCE_RES_BASE,
    MOUNT_TARGET_RES_BASE,
    RESULT_FILENAME,
    MODEL_TIMEOUT
)
import docker
import os


def build_base_image():
    '''
    Build docker image for model execution.

    Args:
        None

    Returns:
        None
    '''
    client = docker.from_env()

    args = {
        "dir": WORKDIR
    }

    dockerfile_path = os.path.join(
        PROJ_ROOT, 'model_usage_manager/build_env/')

    image = client.images.build(
        path=dockerfile_path,
        tag=BASE_IMAGE_NAME,
        buildargs=args,
        nocache=True,
        rm=True
    )


def run_container(model_path, dataset_path, result_id) -> bytes:
    '''
    Run container for model execution from model base image (see BASE_IMAGE_NAME in settings).

    Args:
        model_path: path of model file with perspective to project root
        dataset_path: path of dataset file with perspective to project root
        result_id: id of result (a.k.a. execution) object

    Returns:
        bytes: error logs of the container
    '''

    client = docker.from_env()
    err_logs = b''

    mount_uploads = docker.types.Mount(
        target=MOUNT_TARGET_UPL,
        source=MOUNT_SOURCE_UPL,
        type='bind',
        read_only=True
    )
    mount_result = docker.types.Mount(
        target=os.path.join(MOUNT_TARGET_RES_BASE, result_id),
        source=os.path.join(MOUNT_SOURCE_RES_BASE, result_id),
        type='bind',
        read_only=False
    )

    env = {
        'model_file_path': model_path,
        'dataset_file_path': dataset_path,
        'result_file_path': os.path.join(result_id, RESULT_FILENAME),
        'model_timeout': MODEL_TIMEOUT
    }

    try:
        client.containers.run(
            image=BASE_IMAGE_NAME,
            mounts=[mount_uploads, mount_result],
            environment=env,
            network_disabled=True,
            remove=True,
            stderr=True,
            stdout=False
        )
    except docker.errors.ContainerError as err:
        err_logs = err.stderr

    finally:
        return err_logs
