from core.models import SciModel, Dataset, Execution
from data_hub.celery import app
from data_hub.settings.local import RESULTS_URL, RESULT_FILENAME
from django.contrib.auth.models import User
from model_usage_manager.docker_api import run_container
import os
import pathlib


@app.task
def invoke(model_id, dataset_id, user_id):
    '''
    Celery task which executes sci_model with given dataset and saves execution object.

    Args:
        model_id: id of sci_model to use
        dataset_id: id of dataset to use with sci_model
        user_id: id of user invoking the model

    Returns:
        None
    '''
    model = SciModel.objects.get(pk=model_id)
    dataset = Dataset.objects.get(pk=dataset_id)
    user = User.objects.get(pk=user_id)

    result = Execution(
        user=user,
        dataset=dataset,
        sci_model=model
    )
    # saving withot file to get object id later
    result.save()

    # create directory for result
    pathlib.Path(
        os.path.join(RESULTS_URL, str(result.id))
    ).mkdir(parents=True, exist_ok=True)

    # running container with model and dataset
    logs_bin = run_container(
        model.file.name, dataset.file.name, str(result.id))
    logs = logs_bin.decode()

    result_file_rel_path = os.path.join(
        RESULTS_URL, str(result.id), RESULT_FILENAME)

    # saving results to execution object
    if logs:
        result.succ_status = False
        result.err_log = logs
    else:
        result.result.name = result_file_rel_path
        result.succ_status = True
    result.save()
