from core.models import Dataset
from rest_framework import serializers


class DatasetOutSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Dataset
        fields = ['id', 'name', 'user_id', 'username', 'desc', 'is_private', 'upload_dt']
