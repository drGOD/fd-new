from rest_framework import serializers
from core.models import Execution


class ExecutionSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Execution
        fields = ['id', 'user_id', 'sci_model_id', 'dt',
                  'dataset_id', 'err_log', 'succ_status', ]
