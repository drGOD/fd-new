from rest_framework import serializers
from core.models import SciModel

class SciModelSerializer(serializers.HyperlinkedModelSerializer):

	id = serializers.ReadOnlyField()

	class Meta:
		model = SciModel
		fields = ['id', 'name', 'user_id', 'username', 'desc', 'version', 'price']
		