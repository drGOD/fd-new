from rest_framework import serializers
from core.models import Subscription


class SubscrioptionSerializer(serializers.HyperlinkedModelSerializer):

    id = serializers.ReadOnlyField()

    class Meta:
        model = Subscription
        fields = ['id', 'user_id', 'sci_model_id', 'dt']
