from rest_framework import serializers
from django.contrib.auth.models import User
from core.models import HelperClass


class UserRegisterSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    class Meta:
        model = User
        fields = ['email', 'username', 'password']


class UserPublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name']


class UserPrivateSerializer(serializers.ModelSerializer):
    class Meta:
        model = HelperClass
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'date_joined',
            'balance'
        ]
