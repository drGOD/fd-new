"""
Flile for describing user subscriptions related views.
"""

from django.contrib.auth.models import User
from core.models import Subscription
from core.serializers.subscription_serializers import SubscrioptionSerializer
from core.utils.general import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


@api_view(["GET", ])
def get_user_subscriptions(request, pk):
    """
    Acc: Public
    Des: Get all subscriptions of user
    """
    user = get_object_or_404(User, pk)
    subs = Subscription.objects.all().filter(user=user)
    subs_info = [SubscrioptionSerializer(sub).data for sub in subs]
    return Response(status=200, data=subs_info)


@api_view(["GET", ])
@permission_classes([IsAuthenticated])
def get_current_user_subscriptions(request):
    """
    Acc: Private
    Des: Get current (logged in) user's subscriptions
    """
    subs = Subscription.objects.all().filter(user=request.user)
    subs_info = [SubscrioptionSerializer(sub).data for sub in subs]
    return Response(status=200, data=subs_info)
