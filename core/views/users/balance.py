from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView

from core.utils.access_rights import access_rights_check


class UserBalance(APIView):

    def put(self, request):
        user = User.objects.get(username=request.user)
        user.profile.balance = user.profile.balance + float(request.data['balance'])
        user.save()
        return Response(
            status=200,
            data={'detail': 'Баланс пополнен'}
        )


