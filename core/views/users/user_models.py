"""
File for describing user model related views. 
"""

from core.models import SciModel
from core.serializers.sci_models_serializers import SciModelSerializer
from core.utils.general import get_object_or_404
from django.contrib.auth.models import User
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


@api_view(["GET", ])
def get_user_models(request, pk):
    """
    Acc: Publc 
    Des: Get all user's models  
    """
    user = get_object_or_404(User, pk)
    sci_models = SciModel.objects.all().filter(user=user)

    serializers = [SciModelSerializer(i).data for i in sci_models]
    return Response(data=serializers)


@api_view(["GET", ])
@permission_classes([IsAuthenticated])
def get_current_user_models(request):
    """
    Acc: Private 
    Des: Get current (logged in) user's models  
    """
    user = request.user
    sci_models = SciModel.objects.all().filter(user=user)

    serializers = [SciModelSerializer(i).data for i in sci_models]
    return Response(data=serializers)
