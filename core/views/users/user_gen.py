"""
File for describing user related views.
User model is generalization of scientists and consumers
"""
from core.serializers.user_serializers import (
    UserRegisterSerializer,
    UserPublicSerializer,
    UserPrivateSerializer,
)

from django.contrib.auth.models import User
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly


class UsersList(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request):
        """
        Acc: Public
        Des: Get all registered users
        """
        users = User.objects.all()
        users_info = [
            UserPublicSerializer(user).data
            for user in users
        ]
        return Response(status=200, data=users_info)

    def post(self, request):
        """
        Acc: Public
        Des: Registration
        """
        input_data = request.data
        password = input_data.get('password', None)
        password2 = input_data.get('password2', None)
        if not (password and password2):
            return Response(status=400, data={"detail": "Необходимо заполнить оба поля с паролем"})

        if password != password2:
            return Response(status=400, data={"detail": "Пароли не совпадают"})

        serializer = UserRegisterSerializer(data=input_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        response_data = {
            'email': input_data['email'],
            'username': input_data['username']
        }
        return Response(status=201, data=response_data)


class UserLogged(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        Acc: Private
        Des: Get current (logged in) user profile
        """
        user = User.objects.get(username=request.user)
        res_user = {'id': user.id, 'username': user.username, 'first_name': user.first_name,
                    'last_name': user.last_name, 'email': user.email, 'date_joined': user.date_joined,
                    'balance': user.profile.balance}
        serializer = UserPrivateSerializer(res_user)
        return Response(status=200, data=serializer.data)

    def put(self, request):
        """
        Acc: Private
        Des: Update current user profile
        """
        #
        # TODO: think of fields that can be updated
        #
        fields_to_update = ['first_name', 'last_name']
        user = request.user
        is_updated = False

        # updating fields in loop
        for field in fields_to_update:
            value = request.data.get(field, None)
            if value:
                is_updated = True
                setattr(user, field, value)

        if is_updated:
            user.save()
            return Response(status=200)
        return Response(status=304)

    def delete(self, request):
        """
        Acc: Private
        Des: Delete current user profile
        """
        user = request.user
        user.delete()
        return Response(
            status=200,
            data={'detail': 'your account has been deleted'}
        )


class UsersDetail(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, pk):
        """
        Used for getting user by its primary key.
        Not an actual route.
        """
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404({'detil': 'User not found'})

    def get(self, request, pk):
        """
        Acc: Public
        Des: Get user by primary key
        """
        user = self.get_object(pk)
        serializer = UserPublicSerializer(user)
        return Response(status=200, data=serializer.data)

    def delete(self, request, pk):
        """
        Acc: Admin
        Des: Delete user by id
        """
        pass
