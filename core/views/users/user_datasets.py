"""
File for describing user datasets related views. 
"""

from core.models import Dataset
from core.serializers.dataset_serializers import DatasetOutSerializer
from core.utils.general import get_object_or_404
from django.contrib.auth.models import User
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


@api_view(["GET", ])
def get_user_datasets(request, pk):
    """
    Acc: Private 
    Des: Get all datasets of user  
    """
    user = get_object_or_404(User, pk)
    if request.user == user:
        datasets = Dataset.objects.all().filter(user=user)
    else:
        datasets = Dataset.objects.all().filter(
            user=user,
            is_private=False
        )
    serializers = [DatasetOutSerializer(i).data for i in datasets]
    return Response(data=serializers)


@api_view(["GET", ])
@permission_classes([IsAuthenticated])
def get_current_user_datasets(request):
    """
    Acc: Private 
    Des: Get current (logged in) user's datasets  
    """
    user = request.user
    datasets = Dataset.objects.all().filter(user=user)

    serializers = [DatasetOutSerializer(i).data for i in datasets]
    return Response(data=serializers)
