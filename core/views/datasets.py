"""
File for describing "datasets" related views. 
Datasets are uploaded by consumers and used by sci_models
"""

from django.http import Http404

from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated

from core.forms import DatasetForm
from core.models import Dataset
from django.contrib.auth.models import User
from core.serializers.dataset_serializers import DatasetOutSerializer
from core.utils.general import get_object_or_404, generate_file_response
from core.utils.access_rights import access_rights_check
import os



class DatasetsList(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request):
        """
        Acc: Public
        Des: Returns list of logged in user datasets.
        """
        datasets = Dataset.objects.all().filter(is_private=False)
        res_datasets = []
        for i in datasets:
            user_info = User.objects.filter(id=i.user_id)
            res_datasets.append({'id': i.id, 'name': i.name, 'user_id': i.user_id, 'desc': i.desc,
                                 'is_private': i.is_private, 'upload_dt': i.upload_dt,
                                 'username': list(user_info)[0].username})
        datasets_info = [
            DatasetOutSerializer(dataset).data
            for dataset in res_datasets
        ]
        return Response(status=200, data=datasets_info)

    def post(self, request):
        """
        Acc: Private
        Des: Upload new dataset.
        """
        user = request.user
        form = DatasetForm(request.POST, request.FILES)
        if form.is_valid():
            dataset = form.save(commit=False)
            dataset.user = user
            dataset.save()
            return Response(status=200, data={'detail': "Датасет успешно загружен", "id": dataset.id})
        return Response(status=400, data=form.errors)


class DatasetDetail(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticatedOrReadOnly]
    model = Dataset

    def get(self, request, pk, format=None):
        """
        Acc: Public
        Des: Get dataset by primary key.
        """
        dataset = get_object_or_404(Dataset, pk)
        user_info = User.objects.filter(id=dataset.user_id)
        res_dataset = {'id': dataset.id, 'name': dataset.name, 'user_id': dataset.user_id, 'desc': dataset.desc,
                       'is_private': dataset.is_private, 'upload_dt': dataset.upload_dt,
                       'username': list(user_info)[0].username}
        # if private -> check for owner
        if not res_dataset['is_private'] or request.user == dataset.user:
            serializer = DatasetOutSerializer(res_dataset)
            return Response(status=200, data=serializer.data)
        return Response(status=403)

    @access_rights_check
    def delete(self, request, pk, format=None):
        """
        Acc: Private
        Dec: Delete dataset.
        """
        dataset = get_object_or_404(Dataset, pk)
        dataset.delete()

        return Response(
            status=200,
            data={'detail': 'Датасет успешно удалён'}
        )


class DatasetFile(APIView):
    model = Dataset

    def get(self, request, pk, format=None):
        """
        Acc: Private
        Des: Get model file
        """
        dataset = get_object_or_404(Dataset, pk)
        path = os.getcwd()
        file_path = str(path)+'/'+str(dataset.file)
        response = generate_file_response(file_path)
        return response