"""
File for describing "sci_models" related views.
sci_model is short for scientific model and is uploaded and used by users
"""

from core.models import SciModel, Dataset, Execution
from core.models import SciModel, Subscription

from core.forms import SciModelForm

from core.serializers.sci_models_serializers import SciModelSerializer
from core.serializers.subscription_serializers import SubscrioptionSerializer
from core.serializers.execution_serializers import ExecutionSerializer

from core.utils.access_rights import access_rights_check
from core.utils.general import get_object_or_404, generate_file_response

from django.http import Http404

from model_usage_manager.tasks import invoke

from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.views import APIView
from django.contrib.auth.models import User
import os


class SciModelsSubs(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        """
        Acc: Private
        Des: Subscribe current user to model by primary key
        """
        model = get_object_or_404(SciModel, pk)
        sub_check = Subscription.objects.get_or_none(
            user=request.user,
            sci_model=model
        )

        if sub_check:
            return Response(status=304, data={'detail': 'Подписка уже оформлена'})

        new_sub = Subscription(
            user=request.user,
            sci_model=model
        )
        new_sub.save()
        serializer = SubscrioptionSerializer(new_sub)
        return Response(status=200, data=serializer.data)

    def delete(self, request, pk):
        """
        Acc: Private
        Des: Unsubscribe current user from model by primary key
        """
        model = get_object_or_404(SciModel, pk)
        sub = Subscription.objects.get_or_none(
            user=request.user,
            sci_model=model
        )

        if not sub:
            return Response(status=304, data={'detail': 'Вы не подписаны на данную модель'})

        sub.delete()
        return Response(status=200, data={'detail': 'Вы успешно отписались от модели'})


class SciModelsList(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request):
        """
        Acc: Public
        Des: Returns list of all sci_models.
        """
        sci_models = SciModel.objects.all()
        res_models = []
        for i in sci_models:
            user_info = User.objects.filter(id=i.user_id)
            res_models.append({'id': i.id, 'name': i.name, 'user_id': i.user_id, 'username': list(user_info)[0].username,
                              'desc': i.desc, 'version': i.version, 'price': i.price})
        serializers = [SciModelSerializer(i).data for i in res_models]
        return Response(data=serializers)

    def post(self, request):
        """
        Acc: Private
        Des: Upload new sci_model
        """
        user = request.user
        form = SciModelForm(request.POST, request.FILES)
        if form.is_valid():
            scimodel = form.save(commit=False)
            scimodel.user = user
            scimodel.save()

            return Response(status=200, data={'detail': 'модель загружена успешно', 'id': scimodel.id})
        return Response(status=400, data=form.errors)


class SciModelsDetail(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticatedOrReadOnly]
    model = SciModel

    def get(self, request, pk, format=None):
        """
        Acc: Public
        Des: View fields of sci_model. Private fields will be seen only by owner.
        """
        sci_model = get_object_or_404(SciModel, pk)
        user_info = User.objects.filter(id=sci_model.user_id)
        res_model = {'id': sci_model.id, 'name': sci_model.name, 'user_id': sci_model.user_id,
                     'username': list(user_info)[0].username, 'desc': sci_model.desc, 'version': sci_model.version,
                     'price': sci_model.price}
        serializer = SciModelSerializer(res_model)
        return Response(status=200, data=serializer.data)

    @access_rights_check
    def put(self, request, pk, format=None):
        """
        Acc: Private
        Dec: Edit sci_model
        """
        model = get_object_or_404(SciModel, pk)
        form = SciModelForm(request.POST, request.FILES, instance=model)

        if form.is_valid():
            form.save()
            return Response(status=200,
                            data={'Модель успешно обновлена.'})
        return Response(status=400, data=form.errors)

    @access_rights_check
    def delete(self, request, pk, format=None):
        """
        Acc: Private
        Dec: Delete sci_model
        """
        model = get_object_or_404(SciModel, pk)
        print(model.name)
        model.delete()
        return Response(
            status=200,
            data={'detail': 'Модель успешно удалена'}
        )


class SciModelsUse(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        """
        Acc: Private
        Dec: Use model (as consumer)
        """
        model = get_object_or_404(SciModel, pk)
        dataset_id = request.data.get('dataset_id', None)
        dataset = get_object_or_404(Dataset, dataset_id)

        # adding model usage to celery queue
        invoke.delay(pk, int(dataset_id), request.user.id)

        return Response(status=200, data={'detail': 'Модель начала обработку'})


class ModelFile(APIView):
    permission_classes = [IsAuthenticated]
    model = SciModel

    @access_rights_check
    def get(self, request, pk, format=None):
        """
        Acc: Private
        Des: Get model file
        """
        model = get_object_or_404(SciModel, pk)
        path = os.getcwd()
        file_path = str(path)+'/'+str(model.file)
        response = generate_file_response(file_path)
        return response