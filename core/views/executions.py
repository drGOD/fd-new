"""
File for describing "executions" related views.
Executions are objects, storing information about model execution.
"""

from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework.permissions import IsAuthenticated
from core.serializers.execution_serializers import ExecutionSerializer
from core.models import Execution
from core.utils.general import get_object_or_404, generate_file_response
from core.utils.access_rights import access_rights_check
from rest_framework.response import Response


class ExecutionsList(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """
        Acc: Private
        Des: Returns story of current's user executions
        """
        user = request.user
        executions = Execution.objects.all().filter(user=user)

        serializers = [ExecutionSerializer(i).data for i in executions]
        return Response(data=serializers)


class ExecutionDetail(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [IsAuthenticated]
    model = Execution

    @access_rights_check
    def get(self, request, pk, format=None):
        """
        Acc: Private
        Des: Get execution info
        """
        execution = get_object_or_404(Execution, pk)
        serializer = ExecutionSerializer(execution).data
        return Response(data=serializer)


class ExecutionFile(APIView):
    permission_classes = [IsAuthenticated]
    model = Execution

    @access_rights_check
    def get(self, request, pk, format=None):
        """
        Acc: Private
        Des: Get execution file
        """
        execution = get_object_or_404(Execution, pk)
        file_path = execution.result.name
        response = generate_file_response(file_path)
        return response
