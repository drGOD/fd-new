from django.urls import path, include

from core.views.datasets import DatasetsList, DatasetDetail , DatasetFile
from core.views.executions import ExecutionsList, ExecutionDetail, ExecutionFile
from core.views.sci_models import (
    SciModelsList,
    SciModelsDetail,
    SciModelsUse,
    SciModelsSubs,
    ModelFile
)
from core.views.users.user_gen import (
    UsersList,
    UsersDetail,
    UserLogged,
)
from core.views.users.user_datasets import get_user_datasets, get_current_user_datasets
from core.views.users.user_models import get_user_models, get_current_user_models
from core.views.users.user_subs import get_user_subscriptions, get_current_user_subscriptions


urlpatterns = [
    # see documentation: https://django-rest-auth.readthedocs.io/en/latest/api_endpoints.html
    # used mainly for auth/login/
    path('auth/', include('rest_auth.urls')),

    path('user/', UserLogged.as_view(), name='user_logged'),
    path('users/', UsersList.as_view(), name='user_list'),
    path('users/<int:pk>/', UsersDetail.as_view(), name='user_detail'),

    path('users/<int:pk>/datasets/', get_user_datasets, name='user_datasets'),
    path('users/<int:pk>/models/', get_user_models, name='user_models'),
    path('users/<int:pk>/subs/', get_user_subscriptions, name='user_subs'),
    path(
        'user/datasets/',
        get_current_user_datasets,
        name='current_user_datasets'
    ),
    path(
        'user/models/',
        get_current_user_models,
        name='current_user_models'
    ),
    path(
        'user/subs/',
        get_current_user_subscriptions,
        name='current_user_subs'
    ),

    path('executions/', ExecutionsList.as_view(), name='exetucions_list'),
    path('executions/<int:pk>/', ExecutionDetail.as_view(), name='execution_detail'),
    path('executions/<int:pk>/file/',
         ExecutionFile.as_view(), name='execution_file'),

    path('datasets/', DatasetsList.as_view(), name='datasets_list'),
    path('datasets/<int:pk>/', DatasetDetail.as_view(), name='dataset_detail'),
    path('datasets/<int:pk>/file/', DatasetFile.as_view(), name='dataset_file'),

    path('models/', SciModelsList.as_view(), name='sci_models_list'),
    path('models/<int:pk>/', SciModelsDetail.as_view(), name='sci_models_detail'),
    path(
        'models/<int:pk>/subs/',
        SciModelsSubs.as_view(),
        name='sci_models_subscribe'
    ),
    path('models/<int:pk>/file/',
         ModelFile.as_view(), name='model_file'),
    path(
        'models/<int:pk>/use/',
        SciModelsUse.as_view(),
        name='sci_models_use'
    ),
]