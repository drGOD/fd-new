from core.models import Dataset, SciModel
from django import forms
from core.models import Profile


class DatasetForm(forms.ModelForm):
    class Meta:
        model = Dataset
        exclude = ('user',)


class SciModelForm(forms.ModelForm):
    class Meta:
        model = SciModel
        exclude = ('user', 'upload_dt')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('balance', )