from django.test import TestCase

class TestSubs(TestCase):
    def setUp(self):
        self.test_user = {"username": "User",
                          "email": "user@mail.ru",
                          "first_name": "User",
                          "password": "111",
                          "password2": "111"}
        self.reg = self.client.post('/api/v1/users/', self.test_user)
        self.log_user = {"email": self.test_user["email"],
                         "password": self.test_user["password"]}
        self.log = self.client.post('/api/v1/auth/login/', self.log_user)
        self.file = open("core/tests/test_models.py")
        self.model = {"name": "test model",
                      "desc": "model",
                      "version": 1,
                      "price": 100,
                      "file": self.file}
        self.client.post('/api/v1/models/', self.model,
                                     HTTP_AUTHORIZATION='Token ' + self.log.data["key"])

        self.test_user2 = {"username": "User2",
                     "email": "user2@mail.ru",
                     "first_name": "User2",
                     "password": "222",
                     "password2": "222"}
        self.reg2 = self.client.post('/api/v1/users/', self.test_user2)
        self.log_user2 = {"email": self.test_user2["email"],
                          "password": self.test_user2["password"]}
        self.log2 = self.client.post('/api/v1/auth/login/', self.log_user2)

    def test_subs_post_ok(self):
        get = self.client.get('/api/v1/models/')
        post_subs = self.client.post('/api/v1/models/' + str(get.data[0]["id"]) + "/subs/",
                                     HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        get_user = self.client.get('/api/v1/users/')
        self.assertEqual(post_subs.status_code, 200, msg="Ошибка в POST запроосе при подписке на модель")
        self.assertEqual(get_user.data[1]["id"], post_subs.data["user_id"],
                         msg="Ошибка в POST запросе: подписка от неправильного пользователя")
        self.assertEqual(get.data[0]["id"], post_subs.data["sci_model_id"],
                         msg="Ошибка в POST запросе: подписка на неправильную модель")

        post_subs1 = self.client.post('/api/v1/models/' + str(get.data[0]["id"]) + "/subs/",
                                      HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual(post_subs1.status_code, 304, msg="Пропущена ошибка: подписка на модель уже оформлена")

        test_subs = self.client.get("/api/v1/user/subs/", HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual(test_subs.status_code, 200,
                         msg="Ошибка в GET запросе при получении подписок пользователя: не выполняется запрос")
        self.assertEqual(post_subs.data["id"], test_subs.data[0]["id"],
                         msg="Ошибка в GET запросе: неправильно оформлена подписка")

    def test_subs_post_fail(self):
        post_subs = self.client.post('/api/v1/models/-1/subs/',
                                     HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual(post_subs.status_code, 404, msg="Пропущена ошибка: подписка на несущуствующую модель")

    def test_subs_delete_ok(self):
        get = self.client.get('/api/v1/models/')
        post_subs = self.client.post('/api/v1/models/' + str(get.data[0]["id"]) + "/subs/",
                                     HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        delete_subs = self.client.delete('/api/v1/models/' + str(get.data[0]["id"]) + "/subs/",
                                         HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual(delete_subs.status_code, 200, msg="Ошибка в DELETE запроосе при удалении подписки на модель")

        test_subs = self.client.get("/api/v1/user/subs/", HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual([], test_subs.data, msg="Ошибка в DELETE запросе: подписка не удалилась")

        delete_subs1 = self.client.delete('/api/v1/models/' + str(get.data[0]["id"]) + "/subs/",
                                         HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual(delete_subs1.status_code, 304, msg="Пропущена ошибка: подписка на модель уже удалена")

    def test_subs_delete_fail(self):
        delete_subs = self.client.delete('/api/v1/models/-1/subs/',
                                         HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual(delete_subs.status_code, 404,
                         msg="Пропущена ошибка: удаление подписки на несущуствующую модель")

    def test_user_subs_by_id_ok(self):
        get = self.client.get('/api/v1/models/')
        post_subs = self.client.post('/api/v1/models/' + str(get.data[0]["id"]) + "/subs/",
                                     HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        log_user = {"email": self.test_user["email"],
                    "password": self.test_user["password"]}
        log = self.client.post('/api/v1/auth/login/', log_user)

        get_user = self.client.get('/api/v1/users/')
        test_subs = self.client.get("/api/v1/users/" + str(get_user.data[0]["id"]) + "/subs/",
                                    HTTP_AUTHORIZATION='Token ' + self.log2.data["key"])
        self.assertEqual(test_subs.status_code, 200,
                         msg="Ошибка в GET запросе при получении подписок полоьзователя по его ID")
        self.assertEqual(test_subs.data[0]["id"], post_subs.data["id"],
                         msg="Неправильно получены данные о подписках пользователе по его ID")

    def test_get_user_subs_by_id_fail(self):
        get_subs = self.client.get('/api/v1/users/-1/subs/')
        self.assertEqual(get_subs.status_code, 404,
                         msg="Пропущена ошибка: несуществующий пользователь")