from django.test import TestCase

class TestUserModels(TestCase):
    def setUp(self):
        self.test_user = {"username": "User",
                          "email": "user@mail.ru",
                          "first_name": "User",
                          "password": "111",
                          "password2": "111"}
        self.reg = self.client.post('/api/v1/users/', self.test_user)
        self.log_user = {"email": self.test_user["email"],
                         "password": self.test_user["password"]}
        self.log = self.client.post('/api/v1/auth/login/', self.log_user)
        self.file = open("core/tests/test_models.py")
        self.model = {"name": "test model",
                      "desc": "model",
                      "version": 1,
                      "price": 100,
                      "file": self.file}
        self.client.post('/api/v1/models/', self.model,
                                     HTTP_AUTHORIZATION='Token ' + self.log.data["key"])

    def test_get_user_models(self):
        test_get = self.client.get('/api/v1/user/models/', HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
        self.assertEqual(test_get.status_code, 200,
                         msg="Ошибка в GET запросе при получении модели пользователя: не выполняется запрос")
        self.assertEqual(test_get.data[0]["name"], self.model["name"],
                         msg="Ошибка в GET запросе при получении модели пользователя: не совпадают имена моделей")

    def test_get_user_models_by_id_ok(self):
        test_user = {"username": "User2",
                     "email": "user2@mail.ru",
                     "first_name": "User2",
                     "password": "222",
                     "password2": "222"}
        reg = self.client.post('/api/v1/users/', test_user)
        log_user = {"email": test_user["email"],
                    "password": test_user["password"]}
        log = self.client.post('/api/v1/auth/login/', log_user)

        get = self.client.get('/api/v1/users/')
        get_model = self.client.get('/api/v1/users/' + str(get.data[0]["id"]) + "/models/")
        self.assertEqual(get_model.status_code, 200,
                         msg="Ошибка в GET запросе при получении модели полоьзователя по его ID")
        self.assertEqual(get_model.data[0]["name"], self.model["name"],
                         msg="Неправильно получены данные о моделях пользователе по его ID")

    def test_get_user_models_by_id_fail(self):
        get_model = self.client.get('/api/v1/users/-1/models/')
        self.assertEqual(get_model.status_code, 404,
                         msg="Пропущена ошибка: несуществующий пользователь")
