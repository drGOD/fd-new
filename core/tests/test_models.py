from django.test import TestCase


class TestLoadNewModel(TestCase):
    def setUp(self):
        self.test_user = {"username": "User",
                          "email": "user@mail.ru",
                          "first_name": "User",
                          "password": "111",
                          "password2": "111"}
        self.user = self.test_user
        self.reg = self.client.post('/api/v1/users/', self.test_user)
        self.log_user = {"email": self.user["email"],
                         "password": self.user["password"]}
        self.log = self.client.post('/api/v1/auth/login/', self.log_user)

    def test_load_model_ok(self):
        test_file = open("core/tests/model.py")
        test_model = {"name": "test model",
                      "desc": "model",
                      "version": 1,
                      "price": 100,
                      "file": test_file}
        test_post = self.client.post('/api/v1/models/', test_model,
                                     HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
        self.assertEqual(test_post.status_code, 200, msg="Ошибка в POST запросе при добавлении модели")
        test_get = self.client.get('/api/v1/models/')
        self.assertEqual(test_get.data[0]["name"], test_model["name"],
                         msg="Ошибка в GET запросе при записи модели: неправильная запись")


class TestModelByID(TestCase):
    def setUp(self):
        self.test_user = {"username": "User",
                          "email": "user@mail.ru",
                          "first_name": "User",
                          "password": "111",
                          "password2": "111"}
        self.user = self.test_user
        self.reg = self.client.post('/api/v1/users/', self.test_user)
        self.log_user = {"email": self.user["email"],
                         "password": self.user["password"]}
        self.log = self.client.post('/api/v1/auth/login/', self.log_user)
        self.file = open("core/tests/model.py")
        self.model = {"name": "test model",
                      "desc": "model",
                      "version": 1,
                      "price": 100,
                      "file": self.file}
        self.client.post('/api/v1/models/', self.model,
                         HTTP_AUTHORIZATION='Token ' + self.log.data["key"])

    def test_model_detail_ok(self):
        get = self.client.get('/api/v1/models/')
        test_get = self.client.get('/api/v1/models/' + str(get.data[0]["id"]) + "/")
        self.assertEqual(test_get.status_code, 200, msg="Ошибка в GET запросе при получении информации о модели")
        self.assertEqual(test_get.data["name"], self.model["name"],
                         msg="Неправильно получены данные о модели при запросе деталей")

    def test_model_detail_fail(self):
        test_get = self.client.get('/api/v1/models/-1/')
        self.assertEqual(test_get.status_code, 404, msg="Пропущена ошибка: несуществующий пользователь")

    def test_delete_model(self):
        get = self.client.get('/api/v1/models/')
        test_delete = self.client.delete('/api/v1/models/' + str(get.data[0]["id"]) + "/",
                                         HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
        self.assertEqual(test_delete.status_code, 200, msg="Ошибка в DELETE запросе при удалении модели")
        get = self.client.get('/api/v1/models/' + str(get.data[0]["id"]) + "/")
        self.assertEqual(get.status_code, 404, msg="Модель не была удалена")

    # def test_put_model(self):
    #     get = self.client.get('/api/v1/models/')
    #     test_file = open("core/tests/model.py")
    #     test_model = {"name": "test model 1",
    #                   "desc": "model 1",
    #                   "version": 2,
    #                   "price": 200,
    #                   "file": test_file}
    #     test_put = self.client.put('/api/v1/models/' + str(get.data[0]["id"]) + "/", test_model,
    #                                HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
    #     test_get = self.client.get('/api/v1/models/')
    #     print("error_put", test_put.data)
    #     self.assertEqual(test_put.status_code, 200, msg="Ошибка в PUT запросе при изменении информации о модели")
    #     self.assertEqual(test_get.data[0]["version"], test_model["version"],
    #                      msg="Неправильно изменены данные о модели")

    # def test_use_model(self):
    #     file = open("core/tests/dataset.csv")
    #     dataset = {"name": "test_dataset",
    #                "desc": "dataset",
    #                "is_private": False,
    #                "file": file}
    #     add_dataset = self.client.post('/api/v1/datasets/', dataset,
    #                                    HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
    #     get_dataset = self.client.get('/api/v1/datasets/')
    #
    #     get_model = self.client.get('/api/v1/models/')
    #     js = {"dataset_id": get_dataset.data[0]["id"]}
    #     use_model = self.client.post("/api/v1/models/" + str(get_model.data[0]["id"]) + "/use/", js,
    #                                  HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
    #     self.assertEqual(use_model.status_code, 200, msg="Ошибка в POST запросе при использовании модели на датасете")
