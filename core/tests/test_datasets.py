from django.test import TestCase


class TestLoadNewDataset(TestCase):
    def setUp(self):
        self.test_user = {"username": "Userdfv",
                          "email": "user@mail.ru",
                          "first_name": "User",
                          "password": "111",
                          "password2": "111"}
        self.user = self.test_user
        self.reg = self.client.post('/api/v1/users/', self.test_user)
        self.log_user = {"email": self.user["email"],
                         "password": self.user["password"]}
        self.log = self.client.post('/api/v1/auth/login/', self.log_user)

    def test_load_dataset_ok(self):
        test_file = open("core/tests/dataset.csv")
        test_dataset = {"name": "test_dataset",
                        "desc": "dataset",
                        "is_private": False,
                        "file": test_file}
        test_post = self.client.post('/api/v1/datasets/', test_dataset,
                                     HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
        self.assertEqual(test_post.status_code, 200, msg="Ошибка в POST запросе при добавлении датасета")
        test_get = self.client.get('/api/v1/datasets/')
        self.assertEqual(test_get.data[0]["name"], test_dataset["name"],
                         msg="Ошибка в GET запросе при записи датасета: неправильная запись")


class TestWorkWithDataset(TestCase):
    def setUp(self):
        self.test_user = {"username": "User",
                          "email": "user@mail.ru",
                          "first_name": "User",
                          "password": "111",
                          "password2": "111"}
        self.user = self.test_user
        self.reg = self.client.post('/api/v1/users/', self.test_user)
        self.log_user = {"email": self.user["email"],
                         "password": self.user["password"]}
        self.log = self.client.post('/api/v1/auth/login/', self.log_user)
        self.file = open("core/tests/dataset.csv")
        self.dataset = {"name": "test_dataset",
                        "desc": "dataset",
                        "is_private": False,
                        "file": self.file}
        self.add_dataset = self.client.post('/api/v1/datasets/', self.dataset,
                                            HTTP_AUTHORIZATION='Token ' + self.log.data["key"])

    def test_dataset_detail_ok(self):
        get = self.client.get('/api/v1/datasets/')
        test_get = self.client.get('/api/v1/datasets/' + str(get.data[0]["id"]) + "/",
                                   HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
        self.assertEqual(test_get.status_code, 200, msg="Ошибка в GET запросе при получении информации о датасете")
        self.assertEqual(test_get.data["name"], self.dataset["name"],
                         msg="Неправильно получены данные о датасете при запросе деталей")

    def test_dataset_detail_fail(self):
        test_get = self.client.get('/api/v1/datasets/-1/')
        self.assertEqual(test_get.status_code, 404, msg="Пропущена ошибка: несуществующий пользователь")

    def test_delete_dataset(self):
        get = self.client.get('/api/v1/datasets/')
        test_delete = self.client.delete('/api/v1/datasets/' + str(get.data[0]["id"]) + "/",
                                         HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
        self.assertEqual(test_delete.status_code, 200, msg="Ошибка в DELETE запросе при удалении датасета")
        get = self.client.get('/api/v1/datasets/' + str(get.data[0]["id"]) + "/")
        self.assertEqual(get.status_code, 404, msg= "Датасет не был удален")