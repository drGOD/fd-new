from django.test import TestCase

class TestUserDatasets(TestCase):
    def setUp(self):
        self.test_user = {"username": "User",
                          "email": "user@mail.ru",
                          "first_name": "User",
                          "password": "111",
                          "password2": "111"}
        self.reg = self.client.post('/api/v1/users/', self.test_user)
        self.log_user = {"email": self.test_user["email"],
                         "password": self.test_user["password"]}
        self.log = self.client.post('/api/v1/auth/login/', self.log_user)
        self.file = open("core/tests/dataset.csv")
        self.dataset = {"name": "test_dataset",
                        "desc": "dataset",
                        "is_private": False,
                        "file": self.file}
        self.add_dataset = self.client.post('/api/v1/datasets/', self.dataset,
                                            HTTP_AUTHORIZATION='Token ' + self.log.data["key"])

    def test_get_user_datasets(self):
        test_get = self.client.get('/api/v1/user/datasets/', HTTP_AUTHORIZATION='Token ' + self.log.data["key"])
        self.assertEqual(test_get.status_code, 200,
                         msg="Ошибка в GET запросе при получении даатасета пользователя: не выполняется запрос")
        self.assertEqual(test_get.data[0]["name"], self.dataset["name"],
                         msg="Ошибка в GET запросе при получении датасета пользователя: не совпадают имена датасетов")

    def test_get_user_datasets_by_id_ok(self):
        test_user = {"username": "User2",
                     "email": "user2@mail.ru",
                     "first_name": "User2",
                     "password": "222",
                     "password2": "222"}
        reg = self.client.post('/api/v1/users/', test_user)
        log_user = {"email": test_user["email"],
                    "password": test_user["password"]}
        log = self.client.post('/api/v1/auth/login/', log_user)

        get = self.client.get('/api/v1/users/')
        get_dataset = self.client.get('/api/v1/users/' + str(get.data[0]["id"]) + "/datasets/")
        self.assertEqual(get_dataset.status_code, 200,
                         msg="Ошибка в GET запросе при получении датасета полоьзователя по его ID")
        self.assertEqual(get_dataset.data[0]["name"], self.dataset["name"],
                         msg="Неправильно получены данные о датасетах пользователе по его ID")

    def test_get_user_datasets_by_id_fail(self):
        get_model = self.client.get('/api/v1/users/-1/datasets/')
        self.assertEqual(get_model.status_code, 404,
                         msg="Пропущена ошибка: несуществующий пользователь")
