from django.test import TestCase


class TestRegistration(TestCase):
    """
    Проверка регистрации
    """
    def test_registration_ok(self):
        """
        Проверка успешнной регистрации.
        Отправка POST запроса и его проверка его кода возвращения.
        GET запрс (получение всех пользвателей)
        """
        test_user = {"username": "User",
                     "email": "user@mail.ru",
                     "password": "111",
                     "password2": "111"}
        test_post = self.client.post('/api/v1/users/', test_user)
        test_get = self.client.get('/api/v1/users/')
        self.assertEqual(test_post.status_code, 201, msg="Ошибка в POST запросе при регистрации user")
        self.assertEqual(test_get.status_code, 200, msg="Ошибка в GET запросе при получении всех user")
        self.assertEqual(test_get.data[0]["username"], test_user["username"], msg="Неправильная запись username "
                                                                                  "пользователя при регистрации")

    def test_registration_fail(self):
        test_user = {"username": "User",
                     "email": "user@mail.ru",
                     "first_name": "User",
                     "password": "111",
                     "password2": "112"}
        test_post = self.client.post('/api/v1/users/', test_user)
        self.assertEqual(test_post.status_code, 400, msg="Пропущена ошибка: разные пароли при регистрации")
        test_user["password"] = ""
        test_user["password2"] = ""
        test_post = self.client.post('/api/v1/users/', test_user)
        self.assertEqual(test_post.status_code, 400, msg="Пропущена ошибка: незаполненые пароли при регистрации")


class TestLogin(TestCase):
    def setUp(self):
        test_user = {"username": "User",
                     "email": "user@mail.ru",
                     "first_name": "User",
                     "password": "111",
                     "password2": "111"}
        self.user = test_user
        self.client.post('/api/v1/users/', test_user)

    def test_login_ok(self):
        log_user = {"email": self.user["email"],
                    "password": self.user["password"]}
        log = self.client.post('/api/v1/auth/login/', log_user)
        self.assertEqual(log.status_code, 200, msg="Ошибка в POST запросе при входе")
        test_get = self.client.get('/api/v1/user/', HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(test_get.status_code, 200, msg="GET запрос для получении данных о залогинином полльзователе "
                                                        "НЕ ВЫПОЛНЕН")
        self.assertEqual(test_get.data["email"], log_user["email"], msg="Ошибка в GET запрсе при получении данных о "
                                                                        "залогинином пользователе (email не "
                                                                        "совпадают)")

    def test_login_fail(self):
        log_user = {"email": self.user["email"],
                    "password": self.user["password"] + "1"}
        log = self.client.post('/api/v1/auth/login/', log_user)
        self.assertEqual(log.status_code, 400, msg="Пропущена ошибка: неверный пароль при входе")
        log_user = {"email": "user1@mail.ru",
                    "password": self.user["password"]}
        log = self.client.post('/api/v1/auth/login/', log_user)
        self.assertEqual(log.status_code, 400, msg="Пропущена ошибка: неверный login(email) при входе")
        log_user = {"email": '',
                    "password": ''}
        log = self.client.post('/api/v1/auth/login/', log_user)
        self.assertEqual(log.status_code, 400, msg="Пропущена ошибка: незаполненные данные при входе")

    def test_put_user(self):
        log_user = {"email": self.user["email"],
                    "password": self.user["password"]}
        log = self.client.post('/api/v1/auth/login/', log_user)
        user_info = {"first_name": "User1",
                     "last_name": "UserUser"}
        test_put = self.client.put('/api/v1/user/', user_info, content_type='application/json',
                                   HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(test_put.status_code, 200, msg="Ошибка в PUT запросе при изменении")
        test_get = self.client.get('/api/v1/user/', HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(user_info,
                         {"first_name": test_get.data["first_name"], "last_name": test_get.data["last_name"]},
                         msg="Ошибка в PUT запросе: данные при изменении не записались")

        user_info = {"first_name": "User1",
                     "last_name": ""}
        test_put = self.client.put('/api/v1/user/', user_info, content_type='application/json',
                                   HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(test_put.status_code, 200, msg="Ошибка в PUT запросе при изменении first_name")
        test_get = self.client.get('/api/v1/user/', HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(user_info["first_name"], test_get.data["first_name"],
                         msg="Ошибка в PUT запросе: при изменении только first_name оно неправильно записалось")
        self.assertTrue("" != test_get.data["last_name"], msg="Ошибка в PUT запросе: при изменении только first_name "
                                                              "пустая строока записалась в last_name")

        user_info = {"first_name": "",
                     "last_name": "UserUser1"}
        test_put = self.client.put('/api/v1/user/', user_info, content_type='application/json',
                                   HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(test_put.status_code, 200, msg="Ошибка в PUT запросе при изменении first_name")
        test_get = self.client.get('/api/v1/user/', HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(user_info["last_name"], test_get.data["last_name"],
                         msg="Ошибка в PUT запросе: при изменении только last_name оно неправильно записаось")
        self.assertTrue("" != test_get.data["first_name"], msg="Ошибка в PUT запросе: при изменении только last_name "
                                                               "пустая строока записалась в first_name")

        user_info = {"first_name": "",
                     "last_name": ""}
        test_put = self.client.put('/api/v1/user/', user_info, content_type='application/json',
                                   HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(test_put.status_code, 304, msg="Ошибка в PUT запросе при вводе пустых значений")

    def test_delete_user(self):
        log_user = {"email": self.user["email"],
                    "password": self.user["password"]}
        log = self.client.post('/api/v1/auth/login/', log_user)
        get = self.client.get('/api/v1/user/', HTTP_AUTHORIZATION='Token ' + log.data["key"])
        test_delete = self.client.delete('/api/v1/user/', HTTP_AUTHORIZATION='Token ' + log.data["key"])
        self.assertEqual(test_delete.status_code, 200, msg="Ошибка в DELETE запросе при удалении пользователя")
        get_deleted_user = self.client.get('/api/v1/users/' + str(get.data["id"]) + "/")
        self.assertEqual(get_deleted_user.status_code, 404, msg="Пользователь не был удален")


class TestUserDetail(TestCase):
    def setUp(self):
        test_user = {"username": "User",
                     "email": "user@mail.ru",
                     "first_name": "User",
                     "password": "111",
                     "password2": "111"}
        self.user = test_user
        self.client.post('/api/v1/users/', test_user)

    def test_user_detail_ok(self):
        get = self.client.get('/api/v1/users/')
        test_get = self.client.get('/api/v1/users/'+str(get.data[0]["id"])+"/")
        self.assertEqual(test_get.status_code, 200, msg="Ошибка в GET запросе при получении информации о полоьзователе")
        self.assertEqual(test_get.data["username"], self.user["username"], msg="Неправильно получены данные "
                                                                               "о пользователе при запросе деталей")

    def test_user_detail_fail(self):
        test_get = self.client.get('/api/v1/users/-1/')
        self.assertEqual(test_get.status_code, 404, msg="Пропущена ошибка: несуществующий пользователь")
