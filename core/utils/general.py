from django.http import Http404, HttpResponse
import os


def get_object_or_404(model, pk):
    '''
    Getting django's model object by primary key

    Args:
        model: django Model class
        pk: object's primary key

    Returns:
        object

    Raises:
        Http404 error
    '''
    try:
        return model.objects.get(pk=pk)
    except model.DoesNotExist:
        raise Http404


def generate_file_response(file_path, content_type='text/csv') -> HttpResponse:
    '''
    Generating HttpResponse containing file

    Args:
        file_path: file full path
        content_type: desired content type for response

    Returns:
        HttpResponse

    Raises:
        Http404 in case file can not be reached
    '''
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type=content_type)
            response['Content-Disposition'] = 'attachment; filename=' + \
                os.path.basename(file_path)
            return response
    raise Http404
