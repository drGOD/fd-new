from core.utils.general import get_object_or_404
from rest_framework.response import Response


def access_rights_check(view):
    '''
    Decorator for class-based view function. 
    Needed for checking if user making request is the owner of object that he's thying to access  
    *note:* in order for decorator to work, 'model' class proprty has to be specified

    Args:
        view: class-based private view function  

    Returns:
        decoreted function

    Raises:
        Http404 error
    '''

    def check_wrapper(self, request, pk, *args, **kwargs):
        obj = get_object_or_404(self.model, pk)
        if request.user != obj.user:
            return Response(status=403)

        return view(self, request, pk, *args, **kwargs)
    return check_wrapper
