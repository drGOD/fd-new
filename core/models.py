from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import FileExtensionValidator
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from django.db.models.signals import (post_save, )


def dataset_storage_path(instance, filename):
    return f'static/uploads/datasets/{instance.user.username}/{filename}'


def model_storage_path(instance, filename):
    return f'static/uploads/scimodels/{instance.user.username}/{filename}'


def result_file_storage(instance, filename):
    return f'static/results/{instance.id}/{filename}'


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class ObjectManager(models.Manager):
    def get_or_none(self, *args, **kwargs):
        try:
            return self.get(*args, **kwargs)
        except ObjectDoesNotExist:
            return None


class Dataset(models.Model):
    name = models.CharField(max_length=128)
    file = models.FileField(
        upload_to=dataset_storage_path,
        validators=[FileExtensionValidator(allowed_extensions=['csv'])]
    )
    user = models.ForeignKey(User, models.CASCADE)
    upload_dt = models.DateTimeField(auto_now=True)
    desc = models.CharField(max_length=100000, default="")
    is_private = models.BooleanField(default=False)
    objects = ObjectManager()
    username = ""

class SciModel(models.Model):
    name = models.CharField(max_length=128)
    user = models.ForeignKey(User, models.CASCADE)
    desc = models.CharField(max_length=100000)
    version = models.IntegerField(default=1)
    file = models.FileField(
        upload_to=model_storage_path,
        validators=[FileExtensionValidator(allowed_extensions=['py', 'zip'])]
    )
    price = models.DecimalField(default=0, decimal_places=5, max_digits=100)
    upload_dt = models.DateTimeField(auto_now=True)
    objects = ObjectManager()
    username = ""


class Execution(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    sci_model = models.ForeignKey(SciModel, models.SET_NULL, null=True)
    dataset = models.ForeignKey(Dataset, models.SET_NULL, null=True)
    result = models.FileField(
        upload_to=result_file_storage,
        validators=[FileExtensionValidator(allowed_extensions=['csv'])],
        null=True
    )
    err_log = models.CharField(max_length=10000, null=True)
    succ_status = models.BooleanField(null=True)
    dt = models.DateTimeField(auto_now=True)
    objects = ObjectManager()


class Subscription(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    sci_model = models.ForeignKey(SciModel, models.CASCADE)
    dt = models.DateTimeField(auto_now=True)
    objects = ObjectManager()

    class Meta:
        unique_together = ('user', 'sci_model',)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    balance = models.FloatField(default=0.0)


@receiver(post_save, sender = User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class HelperClass(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    balance = models.FloatField(default=0.0)
    username = ''
    first_name = ''
    last_name = ''
    email = ''
    date_joined = ''